<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vote;

/**
 * Tests the vote activities mod_vote_validate_rank.
 *
 * @package     mod_vote
 * @copyright   University of Nottingham, 2014
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group mod_vote
 * @group uon
 */
final class validate_rank_test extends \advanced_testcase {
    /**
     * Tests that \mod_vote\validate_rank only validates rankings when
     * a set of consecutive ranks have been selected.
     *
     * @covers \mod_vote\validate_rank::add
     * @covers \mod_vote\validate_rank::validate
     * @group mod_vote
     * @group uon
     */
    public function test_validation(): void {
        $this->resetAfterTest(true);

        // Test adding 4 ranked options in rank order.
        $validator = new validate_rank();
        $this->assertTrue($validator->add(1, 10));
        $this->assertTrue($validator->add(2, 11));
        $this->assertTrue($validator->add(3, 12));
        $this->assertTrue($validator->add(4, 13));
        $this->assertTrue($validator->validate());

        // Test that adding 4 ranked options in a random order.
        $validator = new validate_rank();
        $this->assertTrue($validator->add(3, 10));
        $this->assertTrue($validator->add(4, 11));
        $this->assertTrue($validator->add(1, 12));
        $this->assertTrue($validator->add(2, 13));
        $this->assertTrue($validator->validate());

        // Test that adding ranks where 2 options are ranked correctly
        // and 2 options are not ranked have been passed.
        $validator = new validate_rank();
        $this->assertTrue($validator->add(1, 10));
        $this->assertTrue($validator->add(0, 11)); // No rank set.
        $this->assertTrue($validator->add(2, 12));
        $this->assertTrue($validator->add(0, 13));
        $this->assertTrue($validator->validate());

        // Test that adding a duplicate rank fails.
        $validator = new validate_rank();
        $this->assertTrue($validator->add(1, 10));
        $this->assertFalse($validator->add(1, 11));
        $this->assertTrue($validator->validate());

        // Test that missing a consecutive rank fails.
        $validator = new validate_rank();
        $this->assertTrue($validator->add(1, 10));
        $this->assertTrue($validator->add(0, 11)); // No rank set.
        $this->assertTrue($validator->add(3, 12));
        $this->assertTrue($validator->add(0, 13));
        $this->assertFalse($validator->validate());
    }
}
