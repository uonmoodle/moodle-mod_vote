<?php
// This file is part of the vote activity module
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests the vote activity Privacy API implementation.
 *
 * @package     mod_vote
 * @copyright   University of Nottingham, 2018
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_vote\privacy;

/**
 * Tests the vote activity privacy provider class.
 *
 * @package     mod_vote
 * @copyright   University of Nottingham, 2018
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @covers \mod_vote\privacy\provider
 * @group mod_vote
 * @group uon
 */
final class provider_test extends \core_privacy\tests\provider_testcase {
    /** @var \mod_vote_generator The vote data generator. */
    protected $generator;

    /**
     * Setup for each test.
     */
    public function setUp(): void {
        parent::setUp();
        $this->generator = self::getDataGenerator()->get_plugin_generator('mod_vote');
        $this->resetAfterTest(true);
    }

    /**
     * Run at the end of each test.
     */
    public function tearDown(): void {
        $this->assertDebuggingNotCalled();
        parent::tearDown();
    }

    /**
     * Test a user who has not voted.
     */
    public function test_user_with_no_votes(): void {
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $vote = $this->getDataGenerator()->create_module('vote', ['course' => $course->id]);
        // Create a question with two options.
        $question = $this->generator->create_question($vote, [], [[], []]);
        // Vote for the first option created.
        $this->generator->create_votes($otheruser, $question, [$question->options[0]]);
        // Test no contexts are retrived.
        $contextlist = $this->get_contexts_for_userid($user->id, 'mod_vote');
        $contexts = $contextlist->get_contextids();
        $this->assertCount(0, $contexts);
    }

    /**
     * Test a user who voted in a poll.
     */
    public function test_user_voted_in_poll(): void {
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $vote = $this->getDataGenerator()->create_module('vote', ['course' => $course->id, 'votetype' => VOTE_TYPE_POLL]);
        // Create a question with two options.
        $question = $this->generator->create_question($vote, [], [[], []]);
        // Create the votes.
        $this->generator->create_votes($user, $question, [$question->options[0]]);
        $this->generator->create_votes($otheruser, $question, [$question->options[1]]);
        // Test that contexts were retrieved.
        $contextlist = $this->get_contexts_for_userid($user->id, 'mod_vote');
        $contexts = $contextlist->get_contextids();
        $this->assertCount(1, $contexts);

        $cm = get_coursemodule_from_instance('vote', $vote->id);
        $context = \context_module::instance($cm->id);
        $this->assertEquals($context, $contextlist->current());

        // Test export.
        $this->export_context_data_for_user($user->id, $context, 'mod_vote');
        $writer = \core_privacy\local\request\writer::with_context($context);
        $this->assertTrue($writer->has_any_data());
        $subcontext = get_string('privacy:export:votes', 'mod_vote');
        $this->assertTrue($writer->has_any_data([$subcontext]));
        $questioncontext = "$question->id - $question->question";
        $optioncontext = "{$question->options[0]->id} - {$question->options[0]->optionname}";
        $this->assertNotEmpty($writer->get_data([$subcontext, $questioncontext, $optioncontext]));
    }

    /**
     * Test a user who voted in an alternative vote.
     */
    public function test_user_voted_in_av(): void {
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $vote = $this->getDataGenerator()->create_module('vote', ['course' => $course->id, 'votetype' => VOTE_TYPE_AV]);
        $question1 = $this->generator->create_question($vote, [], [[], []]);
        $this->generator->create_votes($user, $question1, [$question1->options[0], $question1->options[1]]);
        $this->generator->create_votes($otheruser, $question1, [$question1->options[1]]);
        $question2 = $this->generator->create_question($vote, [], [[], [], []]);
        $this->generator->create_votes($user, $question2, [$question2->options[2], $question2->options[0]]);
        $this->generator->create_votes($otheruser, $question2, [$question2->options[1], $question2->options[2], $question2->options[0]]);

        // Test that contexts were retrieved.
        $contextlist = $this->get_contexts_for_userid($user->id, 'mod_vote');
        $contexts = $contextlist->get_contextids();
        $this->assertCount(1, $contexts);

        $cm = get_coursemodule_from_instance('vote', $vote->id);
        $context = \context_module::instance($cm->id);
        $this->assertEquals($context, $contextlist->current());

        // Test export.
        $this->export_context_data_for_user($user->id, $context, 'mod_vote');
        $writer = \core_privacy\local\request\writer::with_context($context);
        $this->assertTrue($writer->has_any_data());
        $subcontext = get_string('privacy:export:votes', 'mod_vote');
        $this->assertTrue($writer->has_any_data([$subcontext]));

        $questioncontext1 = "$question1->id - $question1->question";
        $optioncontext1 = "{$question1->options[0]->id} - {$question1->options[0]->optionname}";
        $this->assertNotEmpty($writer->get_data([$subcontext, $questioncontext1, $optioncontext1]));
        $optioncontext2 = "{$question1->options[1]->id} - {$question1->options[1]->optionname}";
        $this->assertNotEmpty($writer->get_data([$subcontext, $questioncontext1, $optioncontext2]));

        $questioncontext2 = "$question2->id - $question2->question";
        $optioncontext3 = "{$question2->options[0]->id} - {$question2->options[0]->optionname}";
        $this->assertNotEmpty($writer->get_data([$subcontext, $questioncontext2, $optioncontext3]));
        $optioncontext4 = "{$question2->options[1]->id} - {$question2->options[1]->optionname}";
        $this->assertEmpty($writer->get_data([$subcontext, $questioncontext2, $optioncontext4]));
        $optioncontext5 = "{$question2->options[2]->id} - {$question2->options[2]->optionname}";
        $this->assertNotEmpty($writer->get_data([$subcontext, $questioncontext2, $optioncontext5]));
    }

    /**
     * Tests that user data is deleted correctly.
     *
     * @param int $votetype
     * @param int $closetime
     * @dataProvider delete_data_for_user_data_deletes_provider
     */
    public function test_delete_data_for_user_data_deletes($votetype, $closetime): void {
        global $DB;
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $vote1params = ['course' => $course->id, 'votetype' => $votetype, 'closedate' => $closetime];
        $vote1 = $this->getDataGenerator()->create_module('vote', $vote1params);
        $vote1cm = get_coursemodule_from_instance('vote', $vote1->id);
        $vote1context = \context_module::instance($vote1cm->id);
        $question1 = $this->generator->create_question($vote1, [], [[], []]);
        $this->generator->create_votes($user, $question1, [$question1->options[0], $question1->options[1]]);
        $this->generator->create_votes($otheruser, $question1, [$question1->options[1]]);
        $vote2 = $this->getDataGenerator()->create_module('vote', ['course' => $course->id]);
        $question2 = $this->generator->create_question($vote2, [], [[], [], []]);
        $this->generator->create_votes($user, $question2, [$question2->options[2], $question2->options[0]]);
        $this->generator->create_votes($otheruser, $question2, [$question2->options[1], $question2->options[2], $question2->options[0]]);
        // Make the call to delete.
        $approvedcontextlist = new \core_privacy\tests\request\approved_contextlist(
            \core_user::get_user($user->id),
            'mod_vote',
            [$vote1context->id]
        );
        provider::delete_data_for_user($approvedcontextlist);
        // Check that the user's data has been deleted.
        $params1 = ['voteid' => $vote1->id, 'userid' => $user->id];
        $this->assertEquals(0, $DB->count_records('vote_votes', $params1));
        $this->assertEquals(1, $DB->count_records('vote_votes', ['voteid' => $vote1->id]));
        // Check the other form has not been affected.
        $this->assertEquals(5, $DB->count_records('vote_votes', ['voteid' => $vote2->id]));
    }

    /**
     * Provides test data for test_delete_data_for_user_data_deletes.
     *
     * @return array
     */
    public function delete_data_for_user_data_deletes_provider(): array {
        $now = time();
        $past = $now - 10;
        $future = $now + DAYSECS;
        return [
            'poll-closed' => [VOTE_TYPE_POLL, $past],
            'poll-open' => [VOTE_TYPE_POLL, $future],
            'vote-open' => [VOTE_TYPE_VOTE, $future],
            'altenative-vote-open' => [VOTE_TYPE_AV, $future],
        ];
    }

    /**
     * Tests that user data is not deleted correctly.
     *
     * @param int $votetype
     * @param int $closetime
     * @dataProvider delete_data_for_user_data_not_deleted_provider
     */
    public function test_delete_data_for_user_data_not_deleted($votetype, $closetime): void {
        global $DB;
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $vote1params = ['course' => $course->id, 'votetype' => $votetype, 'closedate' => $closetime];
        $vote1 = $this->getDataGenerator()->create_module('vote', $vote1params);
        $vote1cm = get_coursemodule_from_instance('vote', $vote1->id);
        $vote1context = \context_module::instance($vote1cm->id);
        $question1 = $this->generator->create_question($vote1, [], [[], []]);
        $this->generator->create_votes($user, $question1, [$question1->options[0], $question1->options[1]]);
        $this->generator->create_votes($otheruser, $question1, [$question1->options[1]]);
        $vote2 = $this->getDataGenerator()->create_module('vote', ['course' => $course->id]);
        $question2 = $this->generator->create_question($vote2, [], [[], [], []]);
        $this->generator->create_votes($user, $question2, [$question2->options[2], $question2->options[0]]);
        $this->generator->create_votes($otheruser, $question2, [$question2->options[1], $question2->options[2], $question2->options[0]]);
        // Make the call to delete.
        $approvedcontextlist = new \core_privacy\tests\request\approved_contextlist(
            \core_user::get_user($user->id),
            'mod_vote',
            [$vote1context->id]
        );
        provider::delete_data_for_user($approvedcontextlist);
        // Check that the user's data has not been deleted.
        $params1 = ['voteid' => $vote1->id, 'userid' => $user->id];
        $this->assertEquals(2, $DB->count_records('vote_votes', $params1));
        $this->assertEquals(3, $DB->count_records('vote_votes', ['voteid' => $vote1->id]));
        // Check the other form has not been affected.
        $this->assertEquals(5, $DB->count_records('vote_votes', ['voteid' => $vote2->id]));
    }

    /**
     * Provides test data for test_delete_data_for_user_data_not_deleted.
     *
     * @return array
     */
    public function delete_data_for_user_data_not_deleted_provider(): array {
        $now = time();
        $past = $now - 10;
        return [
            'vote-closed' => [VOTE_TYPE_VOTE, $past],
            'altenative-vote-closed' => [VOTE_TYPE_AV, $past],
        ];
    }

    /**
     * Test that personal data for all users can be deleted from a context.
     */
    public function test_all_users_deleted_from_context(): void {
        global $DB;
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $vote1params = ['course' => $course->id];
        $vote1 = $this->getDataGenerator()->create_module('vote', $vote1params);
        $vote1cm = get_coursemodule_from_instance('vote', $vote1->id);
        $vote1context = \context_module::instance($vote1cm->id);
        $question1 = $this->generator->create_question($vote1, [], [[], []]);
        $this->generator->create_votes($user, $question1, [$question1->options[0], $question1->options[1]]);
        $this->generator->create_votes($otheruser, $question1, [$question1->options[1]]);
        $vote2 = $this->getDataGenerator()->create_module('vote', ['course' => $course->id]);
        $question2 = $this->generator->create_question($vote2, [], [[], [], []]);
        $this->generator->create_votes($user, $question2, [$question2->options[2], $question2->options[0]]);
        $this->generator->create_votes($otheruser, $question2, [$question2->options[1], $question2->options[2], $question2->options[0]]);
        // Make the call to delete.
        provider::delete_data_for_all_users_in_context($vote1context);
        // All reasons from vote 1 should have been removed.
        $this->assertEquals(0, $DB->count_records('vote_votes', ['voteid' => $vote1->id]));
        // The other vote should be unaffected.
        $this->assertEquals(5, $DB->count_records('vote_votes', ['voteid' => $vote2->id]));
    }

    /**
     * Tests that the provider correctly finds users with personal data in a vote.
     */
    public function test_get_users_in_context(): void {
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $vote1params = ['course' => $course->id];
        $vote1 = $this->getDataGenerator()->create_module('vote', $vote1params);
        $vote1cm = get_coursemodule_from_instance('vote', $vote1->id);
        $vote1context = \context_module::instance($vote1cm->id);
        $question1 = $this->generator->create_question($vote1, [], [[], []]);
        $this->generator->create_votes($user, $question1, [$question1->options[0], $question1->options[1]]);
        $vote2 = $this->getDataGenerator()->create_module('vote', ['course' => $course->id]);
        $question2 = $this->generator->create_question($vote2, [], [[], [], []]);
        $this->generator->create_votes($user, $question2, [$question2->options[2], $question2->options[0]]);
        $this->generator->create_votes($otheruser, $question2, [$question2->options[1], $question2->options[2], $question2->options[0]]);

        // Run the test.
        $userlist = new \core_privacy\local\request\userlist($vote1context, 'vote');
        provider::get_users_in_context($userlist);
        $userids = $userlist->get_userids();
        $this->assertCount(1, $userids);
        $this->assertTrue(in_array($user->id, $userids));
    }

    /**
     * Tests that data for a user in a specific context can be deleted.
     */
    public function test_delete_data_for_users(): void {
        global $DB;
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $vote1params = ['course' => $course->id];
        $vote1 = $this->getDataGenerator()->create_module('vote', $vote1params);
        $vote1cm = get_coursemodule_from_instance('vote', $vote1->id);
        $vote1context = \context_module::instance($vote1cm->id);
        $question1 = $this->generator->create_question($vote1, [], [[], []]);
        $this->generator->create_votes($user, $question1, [$question1->options[0], $question1->options[1]]);
        $this->generator->create_votes($otheruser, $question1, [$question1->options[1]]);
        $vote2 = $this->getDataGenerator()->create_module('vote', ['course' => $course->id]);
        $question2 = $this->generator->create_question($vote2, [], [[], [], []]);
        $this->generator->create_votes($user, $question2, [$question2->options[2], $question2->options[0]]);
        $this->generator->create_votes($otheruser, $question2, [$question2->options[1], $question2->options[2], $question2->options[0]]);

        $userlist = new \core_privacy\local\request\approved_userlist($vote1context, 'vote', [$user->id]);
        provider::delete_data_for_users($userlist);

        // Check that the correct data been deleted.
        $this->assertEquals(1, $DB->count_records('vote_votes', ['voteid' => $vote1->id]));
        $this->assertEquals(1, $DB->count_records('vote_votes', ['voteid' => $vote1->id, 'userid' => $otheruser->id]));
        // Nothing here should have changed.
        $this->assertEquals(5, $DB->count_records('vote_votes', ['voteid' => $vote2->id]));
    }
}
