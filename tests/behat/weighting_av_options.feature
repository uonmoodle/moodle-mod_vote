@mod @mod_vote @uon
Feature: Weighting options on alternative votes during voting.
    In order view an alternate vote correctly
    As a student
    I should see options ordered correctly

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | student1 | Student | 1 | student@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | student1 | C1 | student |
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate |
            | vote | C1 | vote1 | AV test | Alternate my vote | 3 | 1 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | What colour is the sky? |

    Scenario: All the same weighting
        Given the following vote "options" exist:
            | vote | question | optionname | sortorder |
            | vote1 | What colour is the sky? | Green | 1 |
            | vote1 | What colour is the sky? | Blue | 1 |
            | vote1 | What colour is the sky? | Red | 1 |
            | vote1 | What colour is the sky? | Yellow | 1 |
        When I am on the "AV test" "mod_vote > View" page logged in as "student1"
        Then I should see the "What colour is the sky?" question has the following ordered options:
            | Blue |
            | Green |
            | Red |
            | Yellow |

    Scenario: A selection of weightings
        Given the following vote "options" exist:
            | vote | question | optionname | sortorder |
            | vote1 | What colour is the sky? | Green | 1 |
            | vote1 | What colour is the sky? | Blue | 2 |
            | vote1 | What colour is the sky? | Red | 3 |
            | vote1 | What colour is the sky? | Yellow | 2 |
        When I am on the "AV test" "mod_vote > View" page logged in as "student1"
        Then I should see the "What colour is the sky?" question has the following ordered options:
            | Green |
            | Blue |
            | Yellow |
            | Red |
