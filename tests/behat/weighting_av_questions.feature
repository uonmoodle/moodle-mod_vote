@mod @mod_vote @uon
Feature: Weighting options on alternative votes during voting.
    In order view an alternative vote correctly
    As a student
    I should see questions ordered correctly

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | student1 | Student | 1 | student@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | student1 | C1 | student |
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate |
            | vote | C1 | vote1 | AV test | Alternate my vote | 3 | 1 |

    Scenario: All the same weighting
        Given the following vote "questions" exist:
            | vote | question | sortorder |
            | vote1 | What colour is the sky? | 1 |
            | vote1 | A or B? | 1 |
            | vote1 | Do you like this question? | 1 |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | What colour is the sky? | Blue |
            | vote1 | A or B? | C |
            | vote1 | Do you like this question? | Sure |
        When I am on the "AV test" "mod_vote > View" page logged in as "student1"
        Then I should see questions in the following order:
            | A or B? |
            | Do you like this question? |
            | What colour is the sky? |

    Scenario: A selection of weightings
        And the following vote "questions" exist:
            | vote | question | sortorder |
            | vote1 | What colour is the sky? | 2 |
            | vote1 | A or B? | 3 |
            | vote1 | Do you like this question? | 1 |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | What colour is the sky? | Blue |
            | vote1 | A or B? | C |
            | vote1 | Do you like this question? | Sure |
        When I am on the "AV test" "mod_vote > View" page logged in as "student1"
        Then I should see questions in the following order:
            | Do you like this question? |
            | What colour is the sky? |
            | A or B? |
