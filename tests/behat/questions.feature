@mod @mod_vote @uon
Feature: Modifying questions in the vote activity
    In order to create votes
    As an editing teacher
    I need to be able to manipulate questions

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | teacher1 | Teacher | 1 | testteacher@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | teacher1 | C1 | editingteacher |
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate |
            | vote | C1 | vote1 | Vote test | Votes for cash | 2 | 0 |

    Scenario: Create a question
        Given I am on the "Vote test" "mod_vote > View" page logged in as "teacher1"
        When I add a question to the vote and I fill the form with:
            | Question | A really great question |
        Then I should see questions in the following order:
            | A really great question |

    Scenario: Editing a question
        Given the following vote "questions" exist:
            | vote | question |
            | vote1 | What colour is the sky? |
            | vote1 | A or B? |
        And I am on the "Vote test" "mod_vote > View" page logged in as "teacher1"
        When I edit the "What colour is the sky?" question and I fill the form with:
            | Question | Even better question |
        Then I should see questions in the following order:
            | A or B? |
            | Even better question |
        But I should not see "What colour is the sky?"
