@mod @mod_vote @uon
Feature: Modifying vote type (before vote is active)
    When the vote is in editing mode
    As an editing teacher
    I should be able to modify the vote type

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | teacher1 | Teacher | 1 | teacher@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | teacher1 | C1 | editingteacher |

    Scenario Outline: Changing vote type
        Given the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate |
            | vote | C1 | vote1 | Magical mystery vote | One of may voting types | <initialtype> | 0 |
        And I am on the "Magical mystery vote" "mod_vote > View" page logged in as "teacher1"
        And I should see "<initialtest>"
        And I should not see "<finaltest>"
        When I navigate to "Settings" in current page administration
        And I set the field "<fieldname>" to "<newtype>"
        And I press "submitbutton"
        Then I should see "<finaltest>"
        But I should not see "<initialtest>"

        Examples:
            | initialtype | initialtest | fieldname | newtype | finaltest |
            | 1 | Vote type: Poll | id_votetype_2 | Vote | Vote type: Vote |
            | 1 | Vote type: Poll | id_votetype_3 | Alternative vote | Vote type: Alternative vote |
            | 2 | Vote type: Vote | id_votetype_1 | Poll | Vote type: Poll |
            | 2 | Vote type: Vote | id_votetype_3 | Alternative vote | Vote type: Alternative vote |
            | 3 | Vote type: Alternative vote | id_votetype_1 | Poll | Vote type: Poll |
            | 3 | Vote type: Alternative vote | id_votetype_2 | Vote | Vote type: Vote |
