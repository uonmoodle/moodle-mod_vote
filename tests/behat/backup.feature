@uon @mod @mod_vote
Feature: Backup and restore
  In order to backup, duplicate, import and restore a vote activity
  As a teacher
  I need the backup and restore functionality to work

  @javascript
  Scenario: Test back up and restore by duplicating the activity
    Given the following "users" exist:
      | username | firstname | lastname | email                |
      | teacher1 | Darrell   | Teacher1 | teacher1@example.com |
    And the following "courses" exist:
      | fullname | shortname | enablecompletion | showcompletionconditions |
      | Course 1 | C1        | 1                | 1                        |
    And the following "course enrolments" exist:
      | user | course | role           |
      | teacher1 | C1 | editingteacher |
      | teacher1 | C1 | student        |
    And the following "activity" exists:
      | activity        | vote         |
      | course          | C1           |
      | idnumber        | vote1        |
      | name            | Sky colours  |
      | section         | 1            |
      | votestate       | 1            |
      | closedate       | ##tomorrow## |
    And the following vote "questions" exist:
      | vote | question |
      | vote1 | What colour is the sky? |
    And the following vote "options" exist:
      | vote | question | optionname |
      | vote1 | What colour is the sky? | Green |
      | vote1 | What colour is the sky? | Blue |
    And I am on the "C1" "course" page logged in as "teacher1"
    And I turn editing mode on
    When I duplicate "Sky colours" activity
    And I am on the "Sky colours (copy)" "mod_vote > View" page
    # The vote should be created in the editing state, events will only be displayed when the form is active.
    And I click on "Make the vote active" "link_or_button"
    # Test that the events for the activity were created.
    And I am on homepage
    And "Sky colours (copy)" "text" in the "Timeline" "block" should be visible
