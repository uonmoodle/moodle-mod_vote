@mod @mod_vote @uon
Feature: Casting votes in alternative votes
    While an alternative vote is active
    As a student
    I should be able to vote correctly

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | student1 | Student | 1 | student1@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | student1 | C1 | student |
        # votetype 3 is an alternative vote.
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate |
            | vote | C1 | vote1 | AV test | The cake is a lie! | 3 | 1 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | Which desert do you want? |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | Which desert do you want? | Cake |
            | vote1 | Which desert do you want? | Lie |
            | vote1 | Which desert do you want? | Pie |

    Scenario: Cast a vote, ranking all
        Given I am on the "AV test" "mod_vote > View" page logged in as "student1"
        When I fill in the "AV test" vote with:
            | question | vote | rank |
            | Which desert do you want? | Pie | 1 |
            | Which desert do you want? | Cake | 2 |
            | Which desert do you want? | Lie | 3 |
        Then I should see "Thank you for voting"

    @app @javascript
    Scenario: Cast a vote, ranking all in the app
        Given I entered the course "Course 1" as "student1" in the app
        When I press "AV test" in the app
        And I press "Pie" in the app
        And I press "1" in the app
        And I press "OK" in the app
        And I press "Cake" in the app
        And I press "2" in the app
        And I press "OK" in the app
        And I press "Lie" in the app
        And I press "3" in the app
        And I press "OK" in the app
        And I press "Submit vote" in the app
        Then I should find "Thank you for voting, please come back after" in the app

    Scenario: Cast a vote, ranking one
        Given I am on the "AV test" "mod_vote > View" page logged in as "student1"
        When I fill in the "AV test" vote with:
            | question | vote | rank |
            | Which desert do you want? | Pie | 1 |
        Then I should see "Thank you for voting"

    @app @javascript
    Scenario: Cast a vote, ranking one in the app
        Given I entered the course "Course 1" as "student1" in the app
        When I press "AV test" in the app
        And I press "Pie" in the app
        And I press "1" in the app
        And I press "OK" in the app
        And I press "Submit vote" in the app
        Then I should find "Thank you for voting, please come back after" in the app

    Scenario: Duplicated ranks in a question
        Given I am on the "AV test" "mod_vote > View" page logged in as "student1"
        When I fill in the "AV test" vote with:
            | question | vote | rank |
            | Which desert do you want? | Pie | 1 |
            | Which desert do you want? | Lie | 1 |
        Then I should see "You can only select a rank one time per question"
        But I should not see "Thank you for voting"

    Scenario: Ranking a single option, starting rank incorrect
        Given I am on the "AV test" "mod_vote > View" page logged in as "student1"
        When I fill in the "AV test" vote with:
            | question | vote | rank |
            | Which desert do you want? | Pie | 2 |
        Then I should see "You must rank the options consecutively from 1"
        But I should not see "Thank you for voting"

    @app @javascript
    Scenario: Ranking a single option, starting rank incorrect in the app
        Given I entered the course "Course 1" as "student1" in the app
        When I press "AV test" in the app
        And I press "Pie" in the app
        And I press "2" in the app
        And I press "OK" in the app
        And I press "Submit vote" in the app
        Then I should find "Please ensure that you use consecutive rankings starting from 1" in the app
        But I should not find "Thank you for voting" in the app

    Scenario: Gap in the ranking
        Given I am on the "AV test" "mod_vote > View" page logged in as "student1"
        When I fill in the "AV test" vote with:
            | question | vote | rank |
            | Which desert do you want? | Pie | 1 |
            | Which desert do you want? | Cake | 3 |
        Then I should see "You must rank the options consecutively from 1"
        But I should not see "Thank you for voting"

    @app @javascript
    Scenario: Gap in the ranking in the app
        Given I entered the course "Course 1" as "student1" in the app
        When I press "AV test" in the app
        And I press "Pie" in the app
        And I press "1" in the app
        And I press "OK" in the app
        And I press "Cake" in the app
        And I press "3" in the app
        And I press "OK" in the app
        And I press "Submit vote" in the app
        Then I should find "Please ensure that you use consecutive rankings starting from 1" in the app
        But I should not find "Thank you for voting" in the app

    Scenario: No options ranked
        Given I am on the "AV test" "mod_vote > View" page logged in as "student1"
        And I press "submitbutton"
        Then I should see "You must rank at least one option"
        But I should not see "Thank you for voting"

    @app @javascript
    Scenario: Gap in the ranking in the app
        Given I entered the course "Course 1" as "student1" in the app
        When I press "AV test" in the app
        And I press "Submit vote" in the app
        Then I should find "You must vote for an option in this question" in the app
        But I should not find "Thank you for voting" in the app
