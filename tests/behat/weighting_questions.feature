@mod @mod_vote @uon
Feature: Weighting questions.
    In order to manage the display order of questions
    As a teacher
    I need to be able to change the weighting of questions

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | teacher1 | Teacher | 1 | teacher@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | teacher1 | C1 | editingteacher |
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate |
            | vote | C1 | vote1 | Poll test | My little polly | 1 | 0 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | What colour is the sky? |
            | vote1 | A or B? |
            | vote1 | Do you like this question? |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | What colour is the sky? | Green |
            | vote1 | What colour is the sky? | Blue |
            | vote1 | A or B? | A |
            | vote1 | A or B? | B |
            | vote1 | Do you like this question? | Yes |
            | vote1 | Do you like this question? | No |

    Scenario: Editing teacher sets weights on questions
        Given I am on the "Poll test" "mod_vote > View" page logged in as "teacher1"
        And I should see questions in the following order:
            | A or B? |
            | Do you like this question? |
            | What colour is the sky? |
        When I edit the "A or B?" question and I fill the form with:
            | Weighting | 2 |
        Then I should see questions in the following order:
            | Do you like this question? |
            | What colour is the sky? |
            | A or B? |
        When I edit the "What colour is the sky?" question and I fill the form with:
            | Weighting | 2 |
        Then I should see questions in the following order:
            | Do you like this question? |
            | A or B? |
            | What colour is the sky? |
