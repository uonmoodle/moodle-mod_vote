@mod @mod_vote @uon
Feature: Modifying options in the vote activity
    In order to create votes
    As an editing teacher
    I need to be able to manipulate options

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | teacher1 | Teacher | 1 | testteacher@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | teacher1 | C1 | editingteacher |
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate |
            | vote | C1 | vote1 | Vote test | Votes for cash | 2 | 0 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | What colour is the sky? |
            | vote1 | A or B? |

    Scenario: Create an option
        Given I am on the "Vote test" "mod_vote > View" page logged in as "teacher1"
        When I add an option to "What colour is the sky?" and I fill the form with:
            | optionname | Blue |
        Then I should see the "What colour is the sky?" question has the following ordered options:
            | Blue |
        But I should see the "A or B?" question does not have "Blue" option

    Scenario: Editing a option
        Given the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | What colour is the sky? | Green |
            | vote1 | What colour is the sky? | Blue |
            | vote1 | A or B? | Green |
        And I am on the "Vote test" "mod_vote > View" page logged in as "teacher1"
        When I edit the "Green" option in "What colour is the sky?" and I fill the form with:
            | optionname | Red |
        Then I should see the "What colour is the sky?" question has the following ordered options:
            | Blue |
            | Red |
        And I should see the "A or B?" question has the following ordered options:
            | Green |
