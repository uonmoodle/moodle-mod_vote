@mod @mod_vote @uon
Feature: Reseting the vote cache
    When using the activity settings in strange ways
    As an editing teacher
    I should be able to recover if the vote gets into an odd state

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | teacher1 | Teacher | 1 | teacher@example.com |
            | student1 | Student | 1 | student1@example.com |
            | student2 | Student | 2 | student2@example.com |
            | student3 | Student | 3 | student3@example.com |
            | student4 | Student | 4 | student4@example.com |
            | student5 | Student | 5 | student4@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | teacher1 | C1 | editingteacher |
            | student1 | C1 | student |
            | student2 | C1 | student |
            | student3 | C1 | student |
            | student4 | C1 | student |
            | student5 | C1 | student |

    Scenario: Reset alternative vote cache
        Given the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate | closedate |
            | vote | C1 | vote1 | AV test | The cake is a lie! | 3 | 1 | 1 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | Which desert do you want? |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | Which desert do you want? | Cake |
            | vote1 | Which desert do you want? | Lie |
            | vote1 | Which desert do you want? | Pie |
        And the following vote "votes" exist:
            | vote | question | user | optionname | rank |
            | vote1 |  Which desert do you want? | student1 | Cake | 1 |
            | vote1 |  Which desert do you want? | student1 | Lie | 2 |
            | vote1 |  Which desert do you want? | student1 | Pie | 3 |
            | vote1 |  Which desert do you want? | student2 | Cake | 2 |
            | vote1 |  Which desert do you want? | student2 | Lie | 3 |
            | vote1 |  Which desert do you want? | student2 | Pie | 1 |
            | vote1 |  Which desert do you want? | student3 | Cake | 3 |
            | vote1 |  Which desert do you want? | student3 | Lie | 2 |
            | vote1 |  Which desert do you want? | student3 | Pie | 1 |
            | vote1 |  Which desert do you want? | student4 | Cake | 3 |
            | vote1 |  Which desert do you want? | student4 | Lie | 1 |
            | vote1 |  Which desert do you want? | student4 | Pie | 2 |
            | vote1 |  Which desert do you want? | student5 | Cake | 1 |
            | vote1 |  Which desert do you want? | student5 | Lie | 3 |
            | vote1 |  Which desert do you want? | student5 | Pie | 2 |
        And I am on the "AV test" "mod_vote > View" page logged in as "teacher1"
        And I should see "Cake (2 votes)"
        And I should see "Lie (1 votes) Eliminated in round 1"
        And I should see "Pie (3 votes)"
        When I reset the vote cache
        Then I should see "Cake (2 votes)"
        And I should see "Lie (1 votes) Eliminated in round 1"
        And I should see "Pie (3 votes)"

    Scenario Outline: Reset non-alternative vote cache
        Given the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate | closedate |
            | vote | C1 | vote1 | Vote test | The cake is a lie! | <votetype> | 1 | 1 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | Which desert do you want? |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | Which desert do you want? | Cake |
            | vote1 | Which desert do you want? | Lie |
            | vote1 | Which desert do you want? | Pie |
        And the following vote "votes" exist:
            | vote | question | user | optionname |
            | vote1 |  Which desert do you want? | student1 | Cake |
            | vote1 |  Which desert do you want? | student2 | Pie |
            | vote1 |  Which desert do you want? | student3 | Pie |
            | vote1 |  Which desert do you want? | student4 | Lie |
            | vote1 |  Which desert do you want? | student5 | Cake |
        And I am on the "Vote test" "mod_vote > View" page logged in as "teacher1"
        And I should see "Cake (2 votes)"
        And I should see "Lie (1 votes)"
        And I should see "Pie (2 votes)"
        When I reset the vote cache
        Then I should see "Cake (2 votes)"
        And I should see "Lie (1 votes)"
        And I should see "Pie (2 votes)"

        Examples:
            | votetype |
            | 1 |
            | 2 |
