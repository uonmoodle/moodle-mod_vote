@mod @mod_vote @uon
Feature: Closed votes
    In to see the results of a a vote
    As a student
    I should view it after the close time

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | teacher1 | Teacher | 1 | testteacher@example.com |
            | student1 | Student | 1 | student1@example.com |
            | student2 | Student | 2 | student2@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | teacher1 | C1 | editingteacher |
            | student1 | C1 | student |
            | student2 | C1 | student |
        # votetype 2 is a vote.
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate | closedate |
            | vote | C1 | vote1 | Vote test | Votes for cash | 2 | 1 | ##yesterday## |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | What colour is the sky? |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | What colour is the sky? | Green |
            | vote1 | What colour is the sky? | Blue |
            | vote1 | What colour is the sky? | Red |
        And the following vote "votes" exist:
            | vote | question | user | optionname |
            | vote1 |  What colour is the sky? | student1 | Blue |

    Scenario: Students who voted can see the results
        When I am on the "Vote test" "mod_vote > View" page logged in as "student1"
        Then I should see "Blue (1 votes)"
        And I should see "Green (0 votes)"
        And I should see "Red (0 votes)"

    Scenario: Students who did not vote can see the results.
        When I am on the "Vote test" "mod_vote > View" page logged in as "student2"
        Then I should see "Blue (1 votes)"
        And I should see "Green (0 votes)"
        And I should see "Red (0 votes)"
