@mod @mod_vote @uon
Feature: Vote voting
    In order to know what is happening
    As a student
    I need to always see all the votes cast on a poll after I have voted.

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | student1 | Student | 1 | student1@example.com |
            | student2 | Student | 2 | student2@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | student1 | C1 | student |
            | student2 | C1 | student |
        # votetype 1 is a poll
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate |
            | vote | C1 | vote1 | Poll test | My little polly | 1 | 1 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | What colour is the sky? |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | What colour is the sky? | Green |
            | vote1 | What colour is the sky? | Blue |
            | vote1 | What colour is the sky? | Red |

    Scenario: Students should be able to see the results of a poll immediately after voting.
        Given I log in as "student1"
        And I am on "Course 1" course homepage
        And I follow "Poll test"
        When I fill in the "Poll test" vote with:
            | question | vote |
            | What colour is the sky? | Blue |
        Then I should see "Blue (1 votes)"
        And I should see "Green (0 votes)"
        And I should see "Red (0 votes)"

    @app @javascript
    Scenario: Voting in the app
        Given I entered the course "Course 1" as "student1" in the app
        When I press "Poll test" in the app
        And I select "Blue" near "What colour is the sky?" in the app
        And I press "Submit vote" in the app
        Then I should find "Blue (1 votes)" in the app
        And I should find "Green (0 votes)" in the app
        And I should find "Red (0 votes)" in the app
        But I should not find "Thank you for voting, please come back after" in the app

    Scenario: When a student visits a poll after another student has voted they can see the updated results.
        Given I am on the "Poll test" "mod_vote > View" page logged in as "student1"
        And I fill in the "Poll test" vote with:
            | question | vote |
            | What colour is the sky? | Blue |
        And I should see "Blue (1 votes)"
        And I should see "Green (0 votes)"
        And I should see "Red (0 votes)"
        And I log out
        When I am on the "Poll test" "mod_vote > View" page logged in as "student2"
        And I fill in the "Poll test" vote with:
            | question | vote |
            | What colour is the sky? | Red |
        And I should see "Blue (1 votes)"
        And I should see "Green (0 votes)"
        And I should see "Red (1 votes)"
        And I log out
        Then I am on the "Poll test" "mod_vote > View" page logged in as "student1"
        And I should see "Blue (1 votes)"
        And I should see "Green (0 votes)"
        And I should see "Red (1 votes)"
