<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vote;

/**
 * Tests the vote activities \mod_vote\vote class.
 *
 * @package     mod_vote
 * @copyright   University of Nottingham, 2014
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group mod_vote
 */
final class vote_test extends \advanced_testcase {
    /**
     * Tests that \mod_vote\vote works correctly.
     *
     * @covers \mod_vote\vote::get_questions
     * @covers \mod_vote\vote::get_results
     * @covers \mod_vote\vote::has_voted
     * @covers \mod_vote\vote::can_submit
     * @covers \mod_vote\vote::can_edit
     * @covers \mod_vote\vote::results_visible
     * @group mod_vote
     * @group uon
     */
    public function test_renderable(): void {
        global $DB;
        $this->resetAfterTest(true);

        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');

        // Setup some vote activities.
        $course0 = self::getDataGenerator()->create_course();

        $teacherid = $DB->get_field('role', 'id', ['shortname' => 'editingteacher'], MUST_EXIST);
        $studentid = $DB->get_field('role', 'id', ['shortname' => 'student'], MUST_EXIST);

        $user0 = self::getDataGenerator()->create_user();
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $user3 = self::getDataGenerator()->create_user();
        $user4 = self::getDataGenerator()->create_user(); // This user will not vote.

        self::getDataGenerator()->enrol_user($user0->id, $course0->id, $teacherid); // We want one teacher.
        self::getDataGenerator()->enrol_user($user1->id, $course0->id, $studentid);
        self::getDataGenerator()->enrol_user($user2->id, $course0->id, $studentid);
        self::getDataGenerator()->enrol_user($user3->id, $course0->id, $studentid);
        self::getDataGenerator()->enrol_user($user4->id, $course0->id, $studentid);

        // Create a course and add a vote activity to it that closes in the future.
        $vote0 = $votegenerator->create_instance(['course' => $course0->id, 'votetype' => VOTE_TYPE_POLL]);
        $question0 = $votegenerator->create_question(
                $vote0,
                ['question' => 'Test question'],
                [
                    ['optionname' => 'First option'], // Will order alphabetically.
                    ['optionname' => 'Second option'],
                    ['optionname' => 'Third option'],
                    ['optionname' => 'Forth option'],
                ]);
        $votegenerator->create_votes($user0, $question0, [$question0->options[2]]);
        $votegenerator->create_votes($user1, $question0, [$question0->options[0]]);
        $votegenerator->create_votes($user2, $question0, [$question0->options[0]]);
        $votegenerator->create_votes($user3, $question0, [$question0->options[1]]);

        // An AV, that has passed it's close date.
        $vote1 = $votegenerator->create_instance(['course' => $course0->id, 'votetype' => VOTE_TYPE_AV, 'closedate' => (time() - 1000)]);
        $question1 = $votegenerator->create_question(
                $vote1,
                ['question' => 'Test question 2'],
                [
                    ['optionname' => 'Option 1', 'sortorder' => 2], // Should now be last.
                    ['optionname' => 'Option 4'],
                    ['optionname' => 'Option 3'],
                    ['optionname' => 'Option 2'],
                ]);
        $votegenerator->create_votes($user0, $question1, [$question1->options[0], $question1->options[2]]);
        $votegenerator->create_votes($user1, $question1, [$question1->options[1]]);
        $votegenerator->create_votes($user2, $question1, [$question1->options[2]]);
        $votegenerator->create_votes($user3, $question1, [$question1->options[3], $question1->options[2]]);
        // Setup completed.

        // First test with the teacher.
        $this->setUser($user0);
        $renderable0 = new vote($vote0->id);
        $this->assertFalse($renderable0->closed);
        $this->assertTrue($renderable0->can_edit());
        $this->assertFalse($renderable0->can_submit()); // They should not be able to vote, but we have cheated.
        $this->assertTrue($renderable0->has_voted());
        $this->assertTrue($renderable0->results_visible()); // It is a poll and they have a vote.

        $questions = $renderable0->get_questions();
        $this->assertCount(1, $questions);
        $this->assertEquals($question0->id, $questions[0]->id);
        $this->assertEquals($question0->question, $questions[0]->question);
        $this->assertCount(4, $questions[0]->options);
        $this->assertEquals($question0->options[0]->id, $questions[0]->options[0]->id);
        $this->assertEquals($question0->options[0]->optionname, $questions[0]->options[0]->name);
        $this->assertEquals($question0->options[3]->id, $questions[0]->options[1]->id);
        $this->assertEquals($question0->options[3]->optionname, $questions[0]->options[1]->name);
        $this->assertEquals($question0->options[1]->id, $questions[0]->options[2]->id);
        $this->assertEquals($question0->options[1]->optionname, $questions[0]->options[2]->name);
        $this->assertEquals($question0->options[2]->id, $questions[0]->options[3]->id);
        $this->assertEquals($question0->options[2]->optionname, $questions[0]->options[3]->name);

        $results0 = $renderable0->get_results();
        $this->assertCount(1, $results0);
        $this->assertEquals($question0->id, $results0[0]->id);
        $this->assertEquals($question0->question, $results0[0]->question);
        $this->assertEquals(0, $results0[0]->rounds);
        $this->assertEquals(2, $results0[0]->maxresult);
        $this->assertCount(4, $results0[0]->options);
        $this->assertEquals($question0->options[0]->id, $results0[0]->options[0]->id);
        $this->assertEquals($question0->options[0]->optionname, $results0[0]->options[0]->name);
        $this->assertEquals(2, $results0[0]->options[0]->result);
        $this->assertEquals(0, $results0[0]->options[0]->round);
        $this->assertEquals($question0->options[1]->id, $results0[0]->options[1]->id);
        $this->assertEquals($question0->options[1]->optionname, $results0[0]->options[1]->name);
        $this->assertEquals(1, $results0[0]->options[1]->result);
        $this->assertEquals(0, $results0[0]->options[1]->round);
        $this->assertEquals($question0->options[2]->id, $results0[0]->options[2]->id);
        $this->assertEquals($question0->options[2]->optionname, $results0[0]->options[2]->name);
        $this->assertEquals(1, $results0[0]->options[2]->result);
        $this->assertEquals(0, $results0[0]->options[2]->round);
        $this->assertEquals($question0->options[3]->id, $results0[0]->options[3]->id);
        $this->assertEquals($question0->options[3]->optionname, $results0[0]->options[3]->name);
        $this->assertEquals(0, $results0[0]->options[3]->result);
        $this->assertEquals(0, $results0[0]->options[3]->round);

        $renderable1 = new \mod_vote\vote($vote1->id);
        $this->assertTrue($renderable1->closed);
        $this->assertTrue($renderable1->can_edit());
        $this->assertFalse($renderable1->can_submit());
        $this->assertTrue($renderable1->has_voted());
        $this->assertTrue($renderable1->results_visible()); // Close date passed.

        $questions1 = $renderable1->get_questions();
        $this->assertCount(1, $questions1);
        $this->assertEquals($question1->id, $questions1[0]->id);
        $this->assertEquals($question1->question, $questions1[0]->question);
        $this->assertCount(4, $questions1[0]->options);
        $this->assertEquals($question1->options[3]->id, $questions1[0]->options[0]->id);
        $this->assertEquals($question1->options[3]->optionname, $questions1[0]->options[0]->name);
        $this->assertEquals($question1->options[2]->id, $questions1[0]->options[1]->id);
        $this->assertEquals($question1->options[2]->optionname, $questions1[0]->options[1]->name);
        $this->assertEquals($question1->options[1]->id, $questions1[0]->options[2]->id);
        $this->assertEquals($question1->options[1]->optionname, $questions1[0]->options[2]->name);
        $this->assertEquals($question1->options[0]->id, $questions1[0]->options[3]->id);
        $this->assertEquals($question1->options[0]->optionname, $questions1[0]->options[3]->name);

        $results1 = $renderable1->get_results();
        $this->assertCount(1, $results1);
        $this->assertEquals($question1->id, $results1[0]->id);
        $this->assertEquals($question1->question, $results1[0]->question);
        $this->assertEquals(2, $results1[0]->rounds);
        $this->assertEquals(2, $results1[0]->maxresult);
        $this->assertCount(4, $results1[0]->options);
        $this->assertEquals($question1->options[2]->id, $results1[0]->options[0]->id);
        $this->assertEquals($question1->options[2]->optionname, $results1[0]->options[0]->name);
        $this->assertEquals(2, $results1[0]->options[0]->result);
        $this->assertEquals(2, $results1[0]->options[0]->round);
        $this->assertEquals($question1->options[3]->id, $results1[0]->options[1]->id);
        $this->assertEquals($question1->options[3]->optionname, $results1[0]->options[1]->name);
        $this->assertEquals(1, $results1[0]->options[1]->result);
        $this->assertEquals(2, $results1[0]->options[1]->round);
        $this->assertEquals($question1->options[1]->id, $results1[0]->options[2]->id);
        $this->assertEquals($question1->options[1]->optionname, $results1[0]->options[2]->name);
        $this->assertEquals(1, $results1[0]->options[2]->result);
        $this->assertEquals(2, $results1[0]->options[2]->round);
        $this->assertEquals($question1->options[0]->id, $results1[0]->options[3]->id);
        $this->assertEquals($question1->options[0]->optionname, $results1[0]->options[3]->name);
        $this->assertEquals(1, $results1[0]->options[3]->result);
        $this->assertEquals(1, $results1[0]->options[3]->round);

        // Test a student $user1 who has voted.
        $this->setUser($user1);
        $renderable2 = new vote($vote0->id);
        $this->assertFalse($renderable2->closed);
        $this->assertFalse($renderable2->can_edit());
        $this->assertFalse($renderable2->can_submit()); // A user may only vote one time.
        $this->assertTrue($renderable2->has_voted());
        $this->assertTrue($renderable2->results_visible()); // It is a poll and they have a vote.

        $renderable3 = new vote($vote1->id);
        $this->assertTrue($renderable3->closed);
        $this->assertFalse($renderable3->can_edit());
        $this->assertFalse($renderable3->can_submit()); // Voting closed.
        $this->assertTrue($renderable3->has_voted());
        $this->assertTrue($renderable3->results_visible()); // Close date passed.

        // Test a student $user4 who has not voted.
        $this->setUser($user4);
        $renderable4 = new vote($vote0->id);
        $this->assertFalse($renderable4->closed);
        $this->assertFalse($renderable4->can_edit());
        $this->assertTrue($renderable4->can_submit());
        $this->assertFalse($renderable4->has_voted());
        $this->assertFalse($renderable4->results_visible()); // They have not voted.

        $renderable5 = new vote($vote1->id);
        $this->assertTrue($renderable5->closed);
        $this->assertFalse($renderable5->can_edit());
        $this->assertFalse($renderable5->can_submit()); // Voting closed.
        $this->assertFalse($renderable5->has_voted());
        $this->assertTrue($renderable5->results_visible()); // Close date passed.
    }

    /**
     * Tests that the can submit method works with a user parameter.
     */
    public function test_can_submit(): void {
        global $DB;
        $this->resetAfterTest(true);
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');
        $user = self::getDataGenerator()->create_user();
        $votinguser = self::getDataGenerator()->create_user();
        $teacher = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $studentid = $DB->get_field('role', 'id', ['shortname' => 'student'], MUST_EXIST);
        self::getDataGenerator()->enrol_user($user->id, $course->id, $studentid);
        self::getDataGenerator()->enrol_user($votinguser->id, $course->id, $studentid);
        self::getDataGenerator()->enrol_user($teacher->id, $course->id, 'editingteacher');
        // Create a course and add a vote activity to it that closes in the future.
        $activity = $votegenerator->create_instance(['course' => $course->id, 'votetype' => VOTE_TYPE_POLL]);
        $question = $votegenerator->create_question(
                $activity,
                ['question' => 'Test question'],
                [
                    ['optionname' => 'First option'],
                    ['optionname' => 'Second option'],
                    ['optionname' => 'Third option'],
                    ['optionname' => 'Forth option'],
                ]);
        $votegenerator->create_votes($votinguser, $question, [$question->options[2]]);
        // We will not set a user so none of the results should be cached.
        $vote = new vote($activity->id);
        $this->assertFalse($vote->can_submit($votinguser->id)); // Has already voted.
        $this->assertTrue($vote->can_submit($user->id)); // Has not voted yet.
        $this->assertFalse($vote->can_submit($teacher->id)); // Teachers do not have the vote capability.
    }

    /**
     * Tests that the has voted method works with a user parameter.
     */
    public function test_has_voted(): void {
        global $DB;
        $this->resetAfterTest(true);
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');
        $user = self::getDataGenerator()->create_user();
        $votinguser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $studentid = $DB->get_field('role', 'id', ['shortname' => 'student'], MUST_EXIST);
        self::getDataGenerator()->enrol_user($user->id, $course->id, $studentid);
        self::getDataGenerator()->enrol_user($votinguser->id, $course->id, $studentid);
        // Create a course and add a vote activity to it that closes in the future.
        $activity = $votegenerator->create_instance(['course' => $course->id, 'votetype' => VOTE_TYPE_POLL]);
        $question = $votegenerator->create_question(
                $activity,
                ['question' => 'Test question'],
                [
                    ['optionname' => 'First option'],
                    ['optionname' => 'Second option'],
                    ['optionname' => 'Third option'],
                    ['optionname' => 'Forth option'],
                ]);
        $votegenerator->create_votes($votinguser, $question, [$question->options[2]]);
        // We will not set a user so none of the results should be cached.
        $vote = new vote($activity->id);
        $this->assertTrue($vote->has_voted($votinguser->id));
        $this->assertFalse($vote->has_voted($user->id));
    }

    /**
     * Tests if the vote detects that it is avaliable correctly.
     *
     * @param int $state
     * @param bool $expected
     * @return void
     * @dataProvider data_is_available
     */
    public function test_is_available(int $state, bool $expected): void {
        $this->resetAfterTest(true);
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');
        $course = self::getDataGenerator()->create_course();
        $settings = ['course' => $course->id, 'votetype' => VOTE_TYPE_POLL, 'votestate' => $state];
        $activity = $votegenerator->create_instance($settings);
        $vote = new vote($activity->id);
        $this->assertEquals($expected, $vote->is_available());
    }

    /**
     * Data used to test the availability of voting
     *
     * @return array
     */
    public function data_is_available(): array {
        return [
            'editing' => [VOTE_STATE_EDITING, false],
            'active' => [VOTE_STATE_ACTIVE, true],
        ];
    }
}
