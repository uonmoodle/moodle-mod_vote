<?php
// This file is part of the Vote activity.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vote\external;

use core_external\external_api;

/**
 * Tests the vote view logging web service.
 *
 * @package     mod_vote
 * @category    test
 * @copyright   University of Nottingham, 2017
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @runTestsInSeparateProcesses
 * @covers \mod_vote\external\view
 * @group mod_vote
 * @group uon
 */
final class view_test extends \advanced_testcase {
    /**
     * Test that there are no errors when running the web service.
     *
     * @covers \mod_vote\external\view::view
     * @group mod_vote
     * @group uon
     */
    public function test_view(): void {
        global $DB, $USER, $CFG;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('mod_vote');
        $role = $DB->get_record('role', ['shortname' => 'student']);
        $course = self::getDataGenerator()->create_course();
        $student1 = self::getDataGenerator()->create_user();
        $student2 = self::getDataGenerator()->create_user();
        self::getDataGenerator()->enrol_user($student1->id, $course->id, $role->id); // Students.
        self::getDataGenerator()->enrol_user($student2->id, $course->id, $role->id);
        // Setup a Vote.
        $vote = $generator->create_instance(['course' => $course->id]);

        self::setUser($student1);
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = ['id' => $vote->id];
        $result = external_api::call_external_function('mod_vote_view_vote', $args);

        $expectedresult = [
            'status' => true,
            'warnings' => [],
        ];

        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
        $this->assertDebuggingNotCalled();
    }
}
