<?php
// This file is part of the vote plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
//

namespace mod_vote\external;

use core_external\external_function_parameters;
use core_external\external_multiple_structure;
use core_external\external_single_structure;
use core_external\external_value;
use core_external\util;

/**
 * Defines the web service endpoint for getting the details of a Vote activity.
 *
 * @package    mod_vote
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2017 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class details extends \core_external\external_api {
    /**
     * Gets the details of a vote activity.
     *
     * @param int $id The id of a vote activity.
     * @return array
     */
    public static function execute($id) {
        // Get the vote activity and check that the user should have access to it.
        $vote = new \mod_vote\vote($id);
        $context = \context_module::instance($vote->cm->id);
        self::validate_context($context);
        list($voteintro, $voteintroformat) = util::format_text($vote->intro, $vote->introformat,
                $context, 'mod_vote', 'intro', null);
        $return = [
            'id' => $vote->id,
            'title' => $vote->name,
            'intro' => $voteintro,
            'introformat' => $voteintroformat,
            'votetype' => $vote->votetype,
            'closedate' => $vote->closedate,
            'votestate' => $vote->votestate,
            'hasvoted' => $vote->has_voted(),
            'cansubmit' => $vote->can_submit(),
            'questionsvisibile' => $vote->can_submit() || $vote->can_edit(),
            'resultsvisible' => $vote->results_visible(),
            'questions' => [],
        ];
        if ($vote->results_visible()) {
            $return['questions'] = $vote->get_results();
        } else if ($vote->can_submit() || $vote->can_edit()) {
            $return['questions'] = $vote->get_questions();
        }
        return $return;
    }

    /**
     * Defines the inputs for the web service method.
     *
     * @return \core_external\external_function_parameters
     */
    public static function execute_parameters() {
        return new external_function_parameters([
            'id' => new external_value(PARAM_INT, 'The instance id of a vote activity', VALUE_REQUIRED),
        ]);
    }

    /**
     * Defines the output of the web service.
     *
     * @return \core_external\external_function_parameters
     */
    public static function execute_returns() {
        return new external_function_parameters([
            'id' => new external_value(PARAM_INT, 'The id of the activity', VALUE_REQUIRED),
            'title' => new external_value(PARAM_TEXT, 'The name of the activity', VALUE_REQUIRED),
            'intro' => new external_value(PARAM_RAW, 'The description of the activity', VALUE_REQUIRED),
            'introformat' => new external_value(PARAM_INT, 'The format of the description', VALUE_REQUIRED),
            'votetype' => new external_value(PARAM_INT, 'The type of vote', VALUE_REQUIRED),
            'closedate' => new external_value(PARAM_INT, 'UNIX time stamp for when the vote will close', VALUE_REQUIRED),
            'votestate' => new external_value(PARAM_INT, 'The state the vote is in', VALUE_REQUIRED),
            'hasvoted' => new external_value(PARAM_BOOL, 'Flag for if the user has voted', VALUE_REQUIRED),
            'cansubmit' => new external_value(PARAM_BOOL, 'Flag for the ability of the user to submit', VALUE_REQUIRED),
            'questionsvisibile' => new external_value(PARAM_BOOL, 'Flag for the showing questions to the user', VALUE_REQUIRED),
            'resultsvisible' => new external_value(PARAM_BOOL, 'Flag for showing the results to the user', VALUE_REQUIRED),
            'questions' => new external_multiple_structure(
                new external_single_structure([
                    'id' => new external_value(PARAM_INT, 'The id of the slot', VALUE_REQUIRED),
                    'question' => new external_value(PARAM_RAW, 'The title of the slot', VALUE_REQUIRED),
                    'rounds' => new external_value(PARAM_INT, 'The number of result rounds', VALUE_OPTIONAL),
                    'maxresult' => new external_value(PARAM_INT, 'The maximum number of votes for an option', VALUE_OPTIONAL),
                    'options' => new external_multiple_structure(
                        new external_single_structure([
                            'id' => new external_value(PARAM_INT, 'The id of the slot', VALUE_REQUIRED),
                            'name' => new external_value(PARAM_RAW, 'The id of the slot', VALUE_REQUIRED),
                            'result' => new external_value(PARAM_INT, 'The number of votes', VALUE_OPTIONAL),
                            'round' => new external_value(PARAM_INT, 'The counting round the option was eliminated in', VALUE_OPTIONAL),
                            ]
                    ), 'The details of an option in the question'),
                ], 'The details of a question in the vote')
            ),
        ]);
    }
}
