<?php
// This file is part of the vote plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
//

namespace mod_vote\external;

use core_external\external_function_parameters;
use core_external\external_multiple_structure;
use core_external\external_single_structure;
use core_external\external_value;
use core_external\external_warnings;

defined('MOODLE_INTERNAL') || die();

require_once(dirname(__DIR__, 2) . '/lib.php');

/**
 * Defines the web service endpoint for registering a vote.
 *
 * @package    mod_vote
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2017 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class vote extends \core_external\external_api {
    /**
     * Records a users vote.
     *
     * @param int $id The id of a vote activity
     * @param int $type The type of vote that is expected
     * @param array $votes The vote that the user is submitting.
     * @return array
     */
    public static function execute($id, $type, $votes) {
        $voteid = $id;
        $vote = new \mod_vote\vote($voteid);
        $success = true;
        $warnings = [];
        $context = \context_module::instance($vote->cm->id);
        self::validate_context($context);
        self::validate_premissions($vote, $success, $warnings);
        self::validate_type($vote, $success, $warnings, $type);
        if ($vote->votetype == VOTE_TYPE_AV) {
            $record = self::generate_av_record($vote, $success, $warnings, $votes);
        } else {
            $record = self::generate_record($vote, $success, $warnings, $votes);
        }
        self::record_vote($record, $success, $vote);
        // Generate the result.
        $result = [];
        $result['status'] = $success;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Check that the user can submit.
     *
     * @param \mod_vote\vote $vote
     * @param boolean $success
     * @param array $warnings
     * @return void
     */
    protected static function validate_premissions(&$vote, &$success, &$warnings) {
        if (!$vote->can_submit()) {
            $success = false;
            $warnings[] = [
                'item' => 'vote',
                'warningcode' => '1',
                'message' => 'User cannot vote',
            ];
        }
    }

    /**
     * Check that the vote is in the correct mode.
     *
     * @param \mod_vote\vote $vote
     * @param boolean $success
     * @param array $warnings
     * @param int $type the type of vote submitted to the webservice.
     * @return void
     */
    protected static function validate_type(&$vote, &$success, &$warnings, $type) {
        if ($vote->votetype != $type) {
            $success = false;
            $warnings[] = [
                'item' => 'vote',
                'warningcode' => '2',
                'message' => 'Vote type mismatch',
            ];
        }
    }

    /**
     * Adds the details to the record required by all vote types.
     *
     * @param \mod_vote\vote $vote
     * @param array $record
     * @return void
     */
    protected static function generate_common_record(&$vote, &$record) {
        $record['voteid'] = $vote->id;
        $record['vt'] = $vote->votetype;
    }

    /**
     * Converts the web service input into a form usable in the processing of the vote,
     * when in alternative voting mode
     *
     * @param \mod_vote\vote $vote
     * @param boolean $success
     * @param array $warnings
     * @param array $votes The vote parameter passed to the web service
     * @return array
     */
    protected static function generate_av_record(&$vote, &$success, &$warnings, $votes) {
        $record = [];
        self::generate_common_record($vote, $record);
        foreach ($votes as $question) {
            // Start validating the question.
            $validation = new \mod_vote\validate_rank();
            if (!isset($question['options']) || count($question['options']) < 1) {
                $success = false;
                $warnings[] = [
                    'item' => 'question',
                    'itemid' => $question['question'],
                    'warningcode' => '6',
                    'message' => 'Too few rankings',
                ];
                continue;
            }
            foreach ($question['options'] as $option) {
                if (!$validation->add($option['rank'], $option['id'])) {
                    // The rank has been used already.
                    $success = false;
                    $warnings[] = [
                        'item' => 'option',
                        'itemid' => $option['id'],
                        'warningcode' => '3',
                        'message' => 'Option reuses a rank',
                    ];
                }
                $record["option-{$question['question']}-{$option['id']}"] = $option['rank'];
            }
            // Test consecutive ranks have been used.
            if (!$validation->validate()) {
                $success = false;
                $warnings[] = [
                    'item' => 'question',
                    'itemid' => $question['question'],
                    'warningcode' => '4',
                    'message' => 'Invalid ranking',
                ];
            }
        }
        return $record;
    }

    /**
     * Converts the web service input into a form usable in the processing of the vote.
     *
     * @param \mod_vote\vote $vote
     * @param boolean $success
     * @param array $warnings
     * @param array $votes The vote parameter passed to the web service
     * @return array
     */
    protected static function generate_record(&$vote, &$success, &$warnings, $votes) {
        $record = [];
        self::generate_common_record($vote, $record);
        foreach ($votes as $question) {
            // Start validating the question.
            $optioncount = 0;
            if (isset($question['options'])) {
                foreach ($question['options'] as $option) {
                    $record["question-{$question['question']}"] = $option['id'];
                    $optioncount++;
                }
            }
            if ($optioncount > 1) {
                $success = false;
                $warnings[] = [
                    'item' => 'question',
                    'itemid' => $question['question'],
                    'warningcode' => '5',
                    'message' => 'Too many rankings',
                ];
            } else if ($optioncount < 1) {
                $success = false;
                $warnings[] = [
                    'item' => 'question',
                    'itemid' => $question['question'],
                    'warningcode' => '6',
                    'message' => 'Too few rankings',
                ];
            }
        }
        return $record;
    }

    /**
     * Records the user's vote if there are no errors with the vote.
     *
     * @param array $record A record that looks as though it was generated by the voting form.
     * @param boolean $success
     * @param \mod_vote\vote $vote
     * @return void
     */
    protected static function record_vote($record, $success, $vote) {
        if (!$success) {
            return;
        }
        \mod_vote\votelib::process_vote((object)$record);
        // Update completion.
        $completion = new \completion_info($vote->course);
        if ($completion->is_enabled($vote->cm) && $vote->completionvoted) {
            $completion->update_state($vote->cm, COMPLETION_COMPLETE);
        }
    }

    /**
     * Defines the inputs for the web service method.
     *
     * @return \core_external\external_function_parameters
     */
    public static function execute_parameters() {
        return new external_function_parameters([
            'id' => new external_value(PARAM_INT, 'The instance id of a vote activity', VALUE_REQUIRED),
            'type' => new external_value(PARAM_INT, 'The type of vote, used to validate that it has not changed', VALUE_REQUIRED),
            'vote' => new external_multiple_structure(
                new external_single_structure([
                    'question' => new external_value(PARAM_INT, 'The id of question being voted on', VALUE_REQUIRED),
                    'options' => new external_multiple_structure(
                        new external_single_structure([
                            'id' => new external_value(PARAM_INT, 'The id of option', VALUE_REQUIRED),
                            'rank' => new external_value(PARAM_INT, 'The rank given to the option', VALUE_REQUIRED),
                        ]),
                    'The details of how a user voted on the question.', VALUE_OPTIONAL),
                ])
            , 'The details of the user\'s vote', VALUE_OPTIONAL),
        ]);
    }

    /**
     * Defines the output of the web service.
     *
     * @return \core_external\external_function_parameters
     */
    public static function execute_returns() {
        return new external_function_parameters(
            [
                'status' => new external_value(PARAM_BOOL, 'status: true if success'),
                'warnings' => new external_warnings(),
            ]
        );
    }
}
