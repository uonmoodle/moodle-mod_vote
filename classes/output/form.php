<?php
// This file is part of the Vote activity.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderable for Moodle forms used by the vote activity.
 *
 * @package    mod_vote
 * @copyright  2018 Nottingham University
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_vote\output;

/**
 * Renderable for Moodle forms.
 *
 * @package    mod_vote
 * @copyright  2018 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class form implements \renderable, \templatable {
    /** @var \moodleform An instance of a Moodle form. */
    public $form;

    /** @var \mod_vote\vote A vote object that the form is for. */
    public $vote;

    /**
     * Exports the data for use in a template.
     *
     * @param \renderer_base $output
     * @return \stdClass
     */
    public function export_for_template(\renderer_base $output): \stdClass {
        ob_start();
        $this->form->display();
        $formstring = ob_get_contents();
        ob_end_clean();
        return (object) [
            'form' => $formstring,
            'name' => $this->vote->name,
            'intro' => format_module_intro('vote', $this->vote, $this->vote->cm->id),
        ];
    }
}
