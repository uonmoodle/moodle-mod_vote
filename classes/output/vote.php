<?php
// This file is part of the vote activity
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderable vote object.
 *
 * @package    mod_vote
 * @copyright  2018 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_vote\output;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/vote/lib.php');

/**
 * Renderable vote object.
 *
 * @package    mod_vote
 * @copyright  2018 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class vote implements \renderable, \templatable {
    /** @var bool Stores if the user can edit a the vote. */
    public $canedit = false;

    /** @var bool Stores if the user can vote in the poll. */
    public $canvote = false;

    /** @var bool Stores if the vote has passed it's deadline. */
    public $closed = false;

    /** @var int Timestamp when the vote will close. */
    public $closedate;

    /** @var int The course module id of the vote. */
    public $cmid;

    /** @var bool Stores if the results should be displayed to the user. */
    public $displayresults = false;

    /** @var bool Stores if the user has voted. */
    public $hasvoted = false;

    /** @var int The id of the vote activity. */
    public $id;

    /** @var string The formatted intro for the vote activity. */
    public $intro;

    /** @var bool Stores if this is an alternative vote. */
    public $isaltvote = false;

    /** @var bool Stores if this is a poll. */
    public $ispoll = false;

    /** @var bool Stores if this is a standard vote. */
    public $isvote = false;

    /** @var string The name of the vote. */
    public $name;

    /** @var bool Stores if the vote is out of editing mode. */
    public $open = false;

    /** @var \mod_vote\output\question[] The renerables for each question in the vote. */
    public $questions = [];

    /** @var int The type of vote, should match one of the VOTE_TYPE_* constants. */
    public $type;

    /**
     * Exports the data for use in a template.
     *
     * @param \renderer_base $output
     * @return \stdClass
     */
    public function export_for_template(\renderer_base $output): \stdClass {
        $baseurl = new \moodle_url('/mod/vote/view.php', ['id' => $this->cmid]);
        $data = (object) [
            'canedit' => $this->canedit,
            'canvote' => $this->canvote,
            'closed' => $this->closed,
            'closedate' => $this->closedate,
            'cmid' => $this->cmid,
            'displayresults' => $this->displayresults,
            'hasvoted' => $this->hasvoted,
            'id' => $this->id,
            'intro' => $this->intro,
            'isaltvote' => $this->isaltvote,
            'ispoll' => $this->ispoll,
            'isvote' => $this->isvote,
            'name' => $this->name,
            'open' => $this->open,
            'questions' => [],
            'type' => $this->type,
            'activateurl' => new \moodle_url($baseurl, ['f' => VOTE_FUNC_ACTIVATE]),
            'deleteoptionurl' => new \moodle_url($baseurl, ['f' => VOTE_FUNC_OPTION_DELETE]),
            'deletequestionurl' => new \moodle_url($baseurl, ['f' => VOTE_FUNC_QUESTION_DELETE]),
            'optionurl' => new \moodle_url($baseurl, ['f' => VOTE_FUNC_OPTION]),
            'questionurl' => new \moodle_url($baseurl, ['f' => VOTE_FUNC_QUESTION]),
            'reseturl' => new \moodle_url($baseurl, ['f' => VOTE_FUNC_RESET]),
        ];
        foreach ($this->questions as $question) {
            $data->questions[] = $question->export_for_template($output);
        }
        return $data;
    }

    /**
     * Gets the data used in the mobile plugin for this renderable.
     *
     * @return array
     */
    public function get_extra_data(): array {
        $data = [
            'errors' => json_encode(['vote' => [], 'question' => [], 'option' => []]),
        ];
        foreach ($this->questions as $question) {
            $qdata = [
                'question' => $question->id,
                'option' => '0', // Used by Non-AV questions to store the selected option.
                'options' => [], // Used to store AV question responses.
            ];
            foreach ($question->options as $option) {
                $odata = [
                    'id' => $option->id,
                    'rank' => '0',
                ];
                $qdata['options']["o$option->id"] = $odata;
            }
            $data["q$question->id"] = json_encode($qdata);
        }
        return $data;
    }

    /**
     * Gets the JavaScript required for the Mobile app.
     *
     * @return string
     */
    public function get_javascript(): string {
        // The data we need to return depends on the vote type..
        if ($this->type == VOTE_TYPE_AV) {
            $typeprep = <<<JSAV
// Set the ranks for the options.
for (var option in this.CONTENT_OTHERDATA[question].options) {
    var rank = this.CONTENT_OTHERDATA[question].options[option].rank;
    if (rank > 0) {
        // A rank of less than one mean the user has not choosen to rank it.
        var odata = new Object();
        odata.id = this.CONTENT_OTHERDATA[question].options[option].id;
        odata.rank = rank;
        qdata.options.push(odata);
    }
}
JSAV;
        } else {
            $typeprep = <<<JSNONAV
// One option only for non-AV questions.
var odata = new Object();
var oid = this.CONTENT_OTHERDATA[question].option;
// The option id must be greater than 0 for the user to have made a choice.
if (oid > 0) {
    odata.id = oid;
    odata.rank = 1;
    qdata.options.push(odata);
}
JSNONAV;
        }

        // This is the combine JavaScript needed by the plugin.
        $js = <<<JS
/**
 * Converts the other data into a form sutiable for using in the vote
 * parameter of the mod_vote_vote webservice.
 */
this.prep_data = function() {
    var data = [];
    for (var question in this.CONTENT_OTHERDATA) {
        if (question === 'errors') {
            continue;
        }
        // Set the data for the question.
        var qdata = new Object();
        qdata.question = this.CONTENT_OTHERDATA[question].question;
        qdata.options = [];
        $typeprep
        data.push(qdata);
    }
    return data;
};

/**
 * Processes a response from the vote web service.
 */
this.check_response = function(response) {
    // Stores warnings for the whole vote.
    this.CONTENT_OTHERDATA.errors.vote = [];
    // Stores warnings for individual questions.
    this.CONTENT_OTHERDATA.errors.question = new Object();
    // Stores warnings for individual options.
    this.CONTENT_OTHERDATA.errors.option = new Object();
    if (!response.status) {
        for (var warning in response.warnings) {
            // Get the warning data from the web service.
            var item = response.warnings[warning].item;
            var itemid = response.warnings[warning].itemid;
            var warningcode = response.warnings[warning].warningcode;
            var message = response.warnings[warning].message;
            if (item === 'vote') {
                this.CONTENT_OTHERDATA.errors.vote.push('plugin.mod_vote.wserror' + warningcode);
            } else if (item === 'question') {
                if (this.CONTENT_OTHERDATA.errors.question['q' + itemid] === undefined) {
                    this.CONTENT_OTHERDATA.errors.question['q' + itemid] = [];
                }
                this.CONTENT_OTHERDATA.errors.question['q' + itemid].push('plugin.mod_vote.wserror' + warningcode);
            } else if (item === 'option') {
                if (this.CONTENT_OTHERDATA.errors.option['o' + itemid] === undefined) {
                    this.CONTENT_OTHERDATA.errors.option['o' + itemid] = [];
                }
                this.CONTENT_OTHERDATA.errors.option['o' + itemid].push('plugin.mod_vote.wserror' + warningcode);
            }
        }
    } else {
        // All is good, refresh the page to get the new content.
        this.refreshContent();
    }
};

/**
 * Saves the results of the vote.
 *
 * @param params
 */
this.saveData = (params) => {
    this.CoreDomUtilsProvider.showModalLoading().then((modal) => {
        params.vote = this.prep_data();

        this.CoreSitePluginsProvider.callWS('mod_vote_vote', params, {getFromCache: 0, saveToCache: 0}).then((result) => {
            this.check_response(result);
        }).catch((error) => {
            this.CoreDomUtilsProvider.showErrorModalDefault(error, 'core.serverconnection', true);
        }).finally(() => {
            modal.dismiss();
        });
    });
};
JS;
        return $js;
    }
}
