<?php
// This file is part of the vote activity
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderable option object.
 *
 * @package    mod_vote
 * @copyright  2018 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_vote\output;

/**
 * Renderable option object.
 *
 * @package    mod_vote
 * @copyright  2018 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class option implements \renderable, \templatable {
    /** @var int The id of the option. */
    public $id;

    /** @var int The vote counting round option was eliminated in. */
    public $round = 0;

    /** @var string The option's text. */
    public $text;

    /** @var int The number of votes for the option. */
    public $votes = 0;

    /** @var float The percentage of the maximum votes gained by an option in the question. */
    public $percentage = 0.0;

    /** @var int The order of the option. */
    public $position = 0;

    /**
     * Exports the data for use in a template.
     *
     * @param \renderer_base $output
     * @return \stdClass
     */
    public function export_for_template(\renderer_base $output): \stdClass {
        return (object) [
            'oid' => $this->id,
            'round' => $this->round,
            'text' => $this->text,
            'votes' => $this->votes,
            'percentage' => $this->percentage,
            'position' => $this->position,
        ];
    }
}
