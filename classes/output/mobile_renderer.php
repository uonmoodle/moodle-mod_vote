<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines the renderer for the Vote module.
 *
 * @package    mod_vote
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2018 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_vote\output;

/**
 * Defines the renderer for the Vote module.
 *
 * @package    mod_vote
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2018 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mobile_renderer extends \plugin_renderer_base {
    /** @var string The directory the templates are in. */
    protected $dir = 'latest';

    /**
     * Sets up the render correctly for the version of the app.
     *
     * @param int $version
     */
    public function set_version(int $version): void {
        if ($version < 3950) {
            // The app used Ionic3.
            $this->dir = 'ionic3';
        } else if ($version < 45000) {
            // Versions older than 4.5.0 used Ionic 7.
            $this->dir = 'ionic7';
        }
    }

    /**
     * Render a vote renderable.
     *
     * @param \mod_vote\output\vote $vote
     * @return string
     */
    public function render_vote(vote $vote) {
        $data = $vote->export_for_template($this);
        return $this->render_from_template("mod_vote/mobile/{$this->dir}/vote", $data);
    }
}
