<?php
// This file is part of the vote activity
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vote;

defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/formslib.php");

/**
 * Functions used to edit the vote acitivity.
 *
 * @package    mod_vote
 * @copyright  2012 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class editlib {
    /**
     * Takes data submitted from a mod_vote_question_form and enters it into the database.
     *
     * @param stdClass $question - The data from a mod_vote_question_form that was submitted.
     * @throws coding_exception
     */
    public static function process_submittedquestion($question) {
        global $DB;

        if (empty($question->voteid)) {
            throw new \coding_exception('$question->voteid was not set in mod_vote_editlib::process_submittedquestion');
        }

        if (empty($question->question)) {
            throw new \coding_exception('$question->question was not set in mod_vote_editlib::process_submittedquestion');
        }

        if (empty($question->sortorder)) {
            throw new \coding_exception('$question->sortorder was not set in mod_vote_editlib::process_submittedquestion');
        }

        $record = new \stdClass();
        $record->voteid = $question->voteid;
        $record->question = substr($question->question, 0, 255);
        $record->sortorder = $question->sortorder;

        if (!empty($question->q)) { // This is an update.
            $record->id = $question->q;
            $DB->update_record('vote_question', $record);
        } else { // This is an insert.
            $DB->insert_record('vote_question', $record);
        }
    }

    /**
     * Deletse a question and all associated options, votes and cache data from the dataabse.
     *
     * @param int $questionid - The id of the question
     */
    public static function delete_question($questionid) {
        global $DB;
        // Get a list of the optionids for the question.
        $rs = $DB->get_recordset('vote_options', ['questionid' => $questionid], 'id', 'id');
        $optionlist = [];
        foreach ($rs as $record) {
            $optionlist[] = $record->id;
        }
        $optionlist = implode(',', $optionlist);
        $rs->close();

        if ($optionlist != '') { // Only do this if the question has some options.
            // First we must delete any votes for any of the options that the question has.
            $DB->delete_records_select('vote_votes', "optionid IN ($optionlist)", null);

            // We must also delete any entries in the option cache.
            $DB->delete_records_select('vote_result_cache', "optionid IN ($optionlist)", null);

            // We must delete the options for the question now.
            $DB->delete_records('vote_options', ['questionid' => $questionid]);
        }

        // Then we must delete the question itself.
        $DB->delete_records('vote_question', ['id' => $questionid]);
    }



    /**
     * Takes the data submitted via a mod_vote_option_form and adds it to the database.
     *
     * @param stdClass $option - The data from a mod_vote_option_form that was submitted.
     * @throws coding_exception
     */
    public static function process_submittedoption($option) {
        global $DB;

        if (empty($option->voteid)) {
            throw new \coding_exception('$option->voteid was not set in mod_vote_editlib::process_submittedoption');
        }

        if (empty($option->q)) {
            throw new \coding_exception('The question id ($option->q) was not set in mod_vote_editlib::process_submittedoption');
        }

        if (empty($option->optionname)) {
            throw new \coding_exception('$option->optionname was not set in mod_vote_editlib::process_submittedoption');
        }

        if (empty($option->sortorder)) {
            throw new \coding_exception('$option->sortorder was not set in mod_vote_editlib::process_submittedoption');
        }

        $record = new \stdClass();
        $record->voteid = $option->voteid;
        $record->questionid = $option->q;
        $record->optionname = substr($option->optionname, 0, 255);
        $record->sortorder = $option->sortorder;

        if (!empty($option->o)) { // This is an update.
            $record->id = $option->o;
            $DB->update_record('vote_options', $record);
        } else { // This is an insert.
            $DB->insert_record('vote_options', $record);
        }
    }

    /**
     * Deletes an option from the database along with all of it's votes and cache data.
     *
     * @param int $optionid - id of the option to be deleted.
     */
    public static function delete_option($optionid) {
        global $DB;
        // First we must delete any votes for the option.
        $DB->delete_records('vote_votes', ['optionid' => $optionid]);
        // We must also delete it's entry in the option cache.
        $DB->delete_records('vote_result_cache',  ['optionid' => $optionid]);
        // Now delete the option.
        $DB->delete_records('vote_options', ['id' => $optionid]);
    }

    /**
     * Sets the vote to it's active state.
     *
     * @param int $voteid - the id of the vote.
     */
    public static function make_active($voteid) {
        global $DB;
        $record = new \stdClass();
        $record->id = $voteid;
        $record->votestate = VOTE_STATE_ACTIVE;
        $DB->update_record('vote', $record);
    }
}
