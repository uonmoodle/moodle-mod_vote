<?php
// This file is part of the timetable import block
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vote;

/**
 * Class used to precess submitted votes.
 *
 * @package    mod_vote
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2012 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class votelib {
    /**
     * Takes the data submitted via a mod_vote_form and adds it to the database.
     *
     * @param stdClass $votedata - The data from a mod_vote_form the user submitted.
     */
    public static function process_vote($votedata) {
        global $DB, $USER;

        if (empty($votedata->voteid)) {
            throw new \coding_exception('$votedata->voteid not set in mod_vote_votelib::process_vote');
        }

        if (empty($votedata->vt)) {
            throw new \coding_exception('$votedata->vt (vote type) not set in mod_vote_votelib::process_vote');
        }

        $record = new \stdClass();
        $record->voteid = $votedata->voteid;
        $record->userid = $USER->id;

        if ($votedata->vt == VOTE_TYPE_AV) { // It is an AV result.
            foreach ($votedata as $key => $value) {
                $explodedkey = explode('-', $key);
                if ($explodedkey[0] == 'option') { // Only process the submitted options.
                    $record->optionid = $explodedkey[2];
                    $record->vote = $value; // This stores the position rank the user assigned the option.
                    if ($value > 0) { // We do not want to record a value of 0 as it means the option was not ranked.
                        $DB->insert_record('vote_votes', $record);
                    }
                }
            }
        } else { // It is a poll or standard vote.
            $record->vote = 1; // For this type of vote the ranking is always 1.
            foreach ($votedata as $key => $value) {
                if (substr($key, 0, 8) == 'question') { // Only process the submitted questions.
                    $record->optionid = $value;
                    $DB->insert_record('vote_votes', $record);
                }
            }
        }

        // Reset the vote cache.
        cachelib::clear_cache($votedata->voteid);
    }
}
