<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

declare(strict_types=1);

namespace mod_vote;

use core\activity_dates;

/**
 * Activity custom completion date
 *
 * Class for defining mod_vote's custom completion date
 *
 * @package    mod_vote
 * @author     Yijun Xue <yijun.xue@nottingham.ac.uk>
 * @copyright  2022 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class dates extends activity_dates {

    /**
     * Returns dates in mod_vote
     *
     * @return array
     */
    protected function get_dates(): array {
        $timecompletionexpected = $this->cm->completionexpected ?? null;
        $timeclose = $this->cm->customdata['closedate'] ?? null;
        $dates = [];
        $now = time();

        if ($timecompletionexpected) {
            $dates[] = [
                'label' => get_string('completionexpected', 'completion'),
                'timestamp' => (int) $timeclose,
            ];
        }
        if ($timeclose) {
            $closelabelid = $timeclose > $now ? 'activitydate:closes' : 'activitydate:closed';
            $dates[] = [
                'label' => get_string($closelabelid, 'course'),
                'timestamp' => (int) $timeclose,
            ];
        }
        return $dates;
    }
}
