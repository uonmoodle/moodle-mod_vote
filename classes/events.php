<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Maintains events that are used to display information about votes in
 * the course overview.
 *
 * @package    mod_vote
 * @copyright  2018 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_vote;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/calendar/lib.php');

/**
 * Maintains events that are used to display information about votes in
 * the course overview.
 *
 * @package    mod_vote
 * @copyright  2018 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class events {
    /** Close date event name. */
    const CLOSE_DATE = 'closedate';

    /**
     * Get events for the vote.
     *
     * @param int $id
     * @return An array of event records.
     */
    protected static function get_events_for_vote($id) {
        global $DB;
        $filter = [
            'modulename' => 'vote',
            'instance' => $id,
        ];
        return $DB->get_records('event', $filter, 'id ASC');
    }

    /**
     * Creates the data for a close event.
     *
     * @param \stdClass $data Vote editing form data.
     * @return \stdClass
     */
    protected static function generate_close_event_data($data) {
        return (object) [
            'eventtype' => self::CLOSE_DATE,
            'type' => CALENDAR_EVENT_TYPE_ACTION,
            'name' => get_string('calendarclose', 'mod_vote', ['name' => $data->name]),
            'description' => format_module_intro('vote', $data, $data->coursemodule, false),
            'format' => FORMAT_HTML,
            'courseid' => $data->course,
            'groupid' => 0,
            'userid' => 0,
            'modulename' => 'vote',
            'instance' => $data->id,
            'timestart' => $data->closedate,
            'timesort' => $data->closedate,
            'visibile' => instance_is_visible('timetable', $data),
            'timeduration' => 0,
        ];
    }

    /**
     * Creates, updates and deletes events when a vote has been changed.
     *
     * @param \stdClass $data The form data
     * @return void
     */
    public static function update(\stdClass $data) {
        $oldevents = self::get_events_for_vote($data->id);
        // Build an array of all events with updated details.
        $newevents = [
            self::CLOSE_DATE => self::generate_close_event_data($data),
        ];

        // Update existing events.
        foreach ($oldevents as $data) {
            $event = new \calendar_event($data);
            if (isset($newevents[$event->eventtype])) {
                // It is an event we want, so update it.
                $event->update($newevents[$event->eventtype], false);
                unset($newevents[$event->eventtype]);
            } else {
                // No longer needed.
                $event->delete();
            }
        }

        // Create new events.
        foreach ($newevents as $data) {
            \calendar_event::create($data, false);
        }
    }

    /**
     * Deletes events when a vote activity has been removed.
     *
     * @param int $id The id of the vote activity.
     * @return void
     */
    public static function delete($id) {
        $events = self::get_events_for_vote($id);
        foreach ($events as $data) {
            $event = new \calendar_event($data);
            $event->delete();
        }
    }
}
