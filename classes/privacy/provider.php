<?php
// This file is part of the vote activity
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vote\privacy;

use core_privacy\local\metadata\collection;
use core_privacy\local\request\contextlist;
use core_privacy\local\request\approved_contextlist;
use core_privacy\local\request\approved_userlist;
use core_privacy\local\request\writer;
use core_privacy\local\request\userlist;
use core_privacy\local\request\helper;

/**
 * Definition of the data that is stored by the plugin.
 *
 * @see https://docs.moodle.org/dev/Privacy_API
 *
 * @package    mod_vote
 * @category   privacy
 * @copyright  2018 Nottingham University
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class provider implements
        \core_privacy\local\metadata\provider,
        \core_privacy\local\request\plugin\provider,
        \core_privacy\local\request\core_userlist_provider {
    /**
     * Returns meta data about the Vote activity.
     *
     * @param collection $collection The initialised collection to add items to.
     * @return collection A listing of user data stored through this system.
     */
    public static function get_metadata(collection $collection): collection {
        // No sub systems are used.
        // Describes the personal information in the database tables.
        $votes = [
            'optionid' => 'privacy:metadata:vote_votes:optionid',
            'userid' => 'privacy:metadata:vote_votes:userid',
            'vote' => 'privacy:metadata:vote_votes:vote',
        ];
        $collection->add_database_table('vote_votes', $votes, 'privacy:metadata:vote_votes');
        // No user preferences are stored.
        // No data is exported to any external systems.
        return $collection;
    }

    /**
     * Get the list of contexts that contain user information for the specified user.
     *
     * @param int $userid The user to search.
     * @return contextlist $contextlist The contextlist containing the list of contexts used in this plugin.
     */
    public static function get_contexts_for_userid(int $userid): contextlist {
        $contextlist = new contextlist();
        $contextlist->set_component('mod_vote');
        $sql = "SELECT c.id
                  FROM {context} c
            INNER JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel
            INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
            INNER JOIN {vote} v ON v.id = cm.instance
            INNER JOIN {vote_votes} vv ON vv.voteid = v.id AND vv.userid = :userid";
        $params = [
            'contextlevel' => CONTEXT_MODULE,
            'modname' => 'vote',
            'userid' => $userid,
        ];
        $contextlist->add_from_sql($sql, $params);
        return $contextlist;
    }

    /**
     * Export all user data for the specified user, in the specified contexts.
     *
     * @param approved_contextlist $contextlist The approved contexts to export information for.
     */
    public static function export_user_data(approved_contextlist $contextlist) {
        global $DB;

        if (empty($contextlist)) {
            return;
        }

        $user = $contextlist->get_user();
        list($contextsql, $contextparams) = $DB->get_in_or_equal($contextlist->get_contextids(), SQL_PARAMS_NAMED);

        // Build up the data about the votes.
        $sql = "SELECT v.id, c.id AS ctxid
                  FROM {context} c
            INNER JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel
            INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
            INNER JOIN {vote} v ON v.id = cm.instance
                 WHERE c.id $contextsql";
        $params = [
            'contextlevel' => CONTEXT_MODULE,
            'modname' => 'absenceform',
        ];
        $params += $contextparams;
        $votes = $DB->get_recordset_sql($sql, $params);

        foreach ($votes as $vote) {
            $context = \context::instance_by_id($vote->ctxid);
            $data = helper::get_context_data($context, $user);
            writer::with_context($context)
                ->export_data([], $data);
            helper::export_context_files($context, $user);
        }
        static::export_votes($contextlist);
    }

    /**
     * Exports the votes a user has made.
     *
     * @param approved_contextlist $contextlist
     */
    protected static function export_votes(approved_contextlist $contextlist) {
        global $DB;
        $user = $contextlist->get_user();
        list($contextsql, $contextparams) = $DB->get_in_or_equal($contextlist->get_contextids(), SQL_PARAMS_NAMED);

        $sql = "SELECT vv.*, q.id AS qid, q.question, o.optionname, c.id AS ctxid
                  FROM {context} c
            INNER JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel
            INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
            INNER JOIN {vote} v ON v.id = cm.instance
            INNER JOIN {vote_question} q ON q.voteid = v.id
            INNER JOIN {vote_options} o ON o.voteid = v.id AND o.questionid = q.id
            INNER JOIN {vote_votes} vv ON vv.voteid = v.id AND vv.optionid = o.id AND vv.userid = :userid
                 WHERE c.id $contextsql";
        $params = [
            'contextlevel' => CONTEXT_MODULE,
            'modname' => 'vote',
            'userid' => $user->id,
        ];
        $params += $contextparams;
        $votes = $DB->get_recordset_sql($sql, $params);
        $subcontext = get_string('privacy:export:votes', 'mod_vote');
        foreach ($votes as $vote) {
            $context = \context::instance_by_id($vote->ctxid);
            $question = "$vote->qid - $vote->question";
            $option = "$vote->optionid - $vote->optionname";
            $data = (object) [
                'vote' => $vote->vote,
            ];
            writer::with_context($context)->export_data([$subcontext, $question, $option], $data);
        }
    }

    /**
     * Delete all data for all users in the specified context.
     *
     * @param context $context The specific context to delete data for.
     */
    public static function delete_data_for_all_users_in_context(\context $context) {
        global $DB;
        // Check that this is a context_module.
        if (!$context instanceof \context_module) {
            return;
        }
        // Get the course module.
        if (!$cm = get_coursemodule_from_id('vote', $context->instanceid)) {
            return;
        }
        $voteid = $cm->instance;
        $DB->delete_records('vote_votes', ['voteid' => $voteid]);
        // Clear any calculated results.
        \mod_vote\cachelib::clear_cache($voteid);
    }

    /**
     * Delete all user data for the specified user, in the specified contexts.
     *
     * @param approved_contextlist $contextlist The approved contexts and user information to delete information for.
     */
    public static function delete_data_for_user(approved_contextlist $contextlist) {
        global $CFG, $DB;
        require_once($CFG->dirroot . '/mod/vote/lib.php');
        $user = $contextlist->get_user();
        $userid = $user->id;

        list($contextsql, $contextparams) = $DB->get_in_or_equal($contextlist->get_contextids(), SQL_PARAMS_NAMED);

        // We will always delete individual user's data for polls as the results should always be informal.
        // We will only delete an individual's data from votes and alternative votes if the close date,
        // have not yet passed as the results could be formal, so should only be removed by data retention policies
        // as removing the vote result could materially affect by removing the user's vote.
        $sql = "SELECT v.id
                  FROM {context} c
            INNER JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel
            INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
            INNER JOIN {vote} v ON v.id = cm.instance
                 WHERE c.id $contextsql AND (v.votetype = :type OR v.closedate > :time)";
        $params = [
            'contextlevel' => CONTEXT_MODULE,
            'modname' => 'vote',
            'type' => VOTE_TYPE_POLL,
            'time' => time(),
        ];
        $params += $contextparams;
        $votes = $DB->get_recordset_sql($sql, $params);

        foreach ($votes as $vote) {
            $DB->delete_records('vote_votes', ['voteid' => $vote->id, 'userid' => $userid]);
            // Clear any calculated results.
            \mod_vote\cachelib::clear_cache($vote->id);
        }
    }

    /**
     * Get the list of users who have data within a context.
     *
     * @param userlist $userlist The userlist containing the list of users who have data in this context/plugin combination.
     * @return void
     */
    public static function get_users_in_context(userlist $userlist) {
        $context = $userlist->get_context();
        if ($context->contextlevel != CONTEXT_MODULE) {
            return;
        }
         $sql = "SELECT vv.userid
                  FROM {context} c
            INNER JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel AND c.id = :context
            INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
            INNER JOIN {vote} v ON v.id = cm.instance
            INNER JOIN {vote_votes} vv ON vv.voteid = v.id";
        $params = [
            'contextlevel' => CONTEXT_MODULE,
            'modname' => 'vote',
            'context' => $context->id,
        ];
        $userlist->add_from_sql('userid', $sql, $params);
    }

    /**
     * Delete data for multiple users within a single context.
     *
     * @param approved_userlist $userlist The approved context and user information to delete information for.
     * @return void
     */
    public static function delete_data_for_users(approved_userlist $userlist) {
        global $DB;
        $context = $userlist->get_context();
        if ($context->contextlevel != CONTEXT_MODULE) {
            return;
        }
        // Make sure it is a vote context.
        if (!$cm = get_coursemodule_from_id('vote', $context->instanceid)) {
            return;
        }
        list($sql, $params) = $DB->get_in_or_equal($userlist->get_userids(), SQL_PARAMS_NAMED);
        $params['vote'] = $cm->instance;
        // In this case we will delete data regardless of the state of votes and alternative votes as this
        // method is used by the data retention policies. If multiple user roles can vote in them and they do
        // not have the same retention policy the results will not be accurate after the first role has it's data deleted.
        $DB->delete_records_select('vote_votes', "voteid = :vote AND userid $sql", $params);
        \mod_vote\cachelib::clear_cache($cm->instance);
    }
}
