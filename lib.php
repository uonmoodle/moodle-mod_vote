<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants for module vote
 *
 * All the core Moodle functions, neeeded to allow the module to work
 * integrated in Moodle should be placed here.
 * All the vote specific functions, needed to implement all the module
 * logic, should go to locallib.php. This will help to save some memory when
 * Moodle is performing actions across all modules.
 *
 * @package    mod_vote
 * @copyright  2012 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use mod_vote\events;

// The types of vote.
define('VOTE_TYPE_POLL', 1);
define('VOTE_TYPE_VOTE', 2);
define('VOTE_TYPE_AV', 3);

// The states a vote can be in.
define('VOTE_STATE_EDITING', 0);
define('VOTE_STATE_ACTIVE', 1);

// Functions that can be passed by parameter to the vote.
define('VOTE_FUNC_QUESTION', 1);
define('VOTE_FUNC_QUESTION_DELETE', 2);
define('VOTE_FUNC_OPTION', 3);
define('VOTE_FUNC_OPTION_DELETE', 4);
define('VOTE_FUNC_RESET', 5);
define('VOTE_FUNC_VOTE', 6);
define('VOTE_FUNC_ACTIVATE', 7);

define('VOTE_CACHE_TIME', 60); // The cache will be valid for 1 minute.

// Moodle core API.

/**
 * Returns the information on whether the module supports a feature
 *
 * @see plugin_supports() in lib/moodlelib.php
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed true if the feature is supported, null if unknown
 */
function vote_supports($feature) {
    switch($feature) {
        case FEATURE_MOD_INTRO:
        case FEATURE_COMPLETION_TRACKS_VIEWS:
        case FEATURE_COMPLETION_HAS_RULES:
        case FEATURE_BACKUP_MOODLE2:
            return true;
        case FEATURE_GRADE_HAS_GRADE:
        case FEATURE_GRADE_OUTCOMES:
        case FEATURE_RATE:
        case FEATURE_COMMENT:
        case FEATURE_GROUPINGS:
        case FEATURE_GROUPS:
            return false;
        case FEATURE_MOD_PURPOSE:
            return MOD_PURPOSE_COMMUNICATION;
        default:
            return null;
    }
}

/**
 * Saves a new instance of the vote into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param object $vote An object from the form in mod_form.php
 * @param mod_vote_mod_form $mform
 * @return int The id of the newly inserted vote record
 */
function vote_add_instance(stdClass $vote, mod_vote_mod_form $mform = null) {
    global $DB;

    $vote->timecreated = time();
    $vote->id = $DB->insert_record('vote', $vote);
    events::update($vote);
    return $vote->id;
}

/**
 * Updates an instance of the vote in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param object $vote An object from the form in mod_form.php
 * @param mod_vote_mod_form $mform
 * @return boolean Success/Fail
 */
function vote_update_instance(stdClass $vote, mod_vote_mod_form $mform = null) {
    global $DB;

    $vote->timemodified = time();
    $vote->id = $vote->instance;
    $DB->update_record('vote', $vote);
    events::update($vote);
    return true;
}

/**
 * This standard function will check all instances of this module
 * and make sure there are up-to-date events created for each of them.
 * If courseid = 0, then every vote event in the site is checked, else
 * only vote events belonging to the course specified are checked.
 *
 * @param int $courseid
 * @param int|stdClass $instance Vote module instance or ID.
 * @param int|stdClass $cm Course module object or ID (not used in this module).
 * @return bool
 */
function vote_refresh_events($courseid = 0, $instance = null, $cm = null) {
    global $DB;

    $params = [];
    $where = '';

    // Check if we are filtering by course.
    if (!empty($courseid)) {
        if (!is_numeric($courseid)) {
            return false;
        }
        $params['course'] = $courseid;
    }

    // Check if we are finding a specific instance.
    if (!empty($instance)) {
        // Convert the instance into an id.
        if (is_numeric($instance)) {
            $instanceid = $instance;
        } else if (!empty($instance->id)) {
            $instanceid = $instance->id;
        } else {
            return false;
        }
        $params['id'] = $instanceid;
    }

    // Build the query parameter SQL.
    if (!empty($pramas)) {
        $paramkeys = array_keys($params);
        $fragments = [];
        foreach ($paramkeys as $key) {
            $fragments[] = "e.$key = :$key";
        }

        $where = 'AND ' . implode( ' AND ', $fragments);
    }

    $params['module'] = 'vote';

    $sql = "SELECT e.*, cm.id AS coursemodule, cm.visible
              FROM {vote} e
              JOIN {course_modules} cm ON cm.instance = e.id
              JOIN {modules} m ON m.id = cm.module
             WHERE m.name = :module $where";

    $votes = $DB->get_records_sql($sql, $params);

    foreach ($votes as $vote) {
        events::update($vote);
    }

    return true;
}

/**
 * Removes an instance of the vote from the database
 *
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function vote_delete_instance($id) {
    global $DB;

    if (! $vote = $DB->get_record('vote', ['id' => $id])) {
        return false;
    }

    events::delete($id);
    $DB->delete_records('vote_result_cache', ['voteid' => $vote->id]);
    $DB->delete_records('vote_votes', ['voteid' => $vote->id]);
    $DB->delete_records('vote_options', ['voteid' => $vote->id]);
    $DB->delete_records('vote_question', ['voteid' => $vote->id]);

    $DB->delete_records('vote', ['id' => $vote->id]);

    return true;
}

/**
 * Given a course and a time, this module should find recent activity
 * that has occurred in vote activities and print it out.
 * Return true if there was output, or false is there was none.
 *
 * @return boolean
 */
function vote_print_recent_activity($course, $viewfullnames, $timestart) {
    return false; // True if anything was printed, otherwise false.
}

/**
 * Prepares the recent activity data
 *
 * This callback function is supposed to populate the passed array with
 * custom activity records. These records are then rendered into HTML via
 * {@link vote_print_recent_mod_activity()}.
 *
 * @param array $activities sequentially indexed array of objects with the 'cmid' property
 * @param int $index the index in the $activities to use for the next record
 * @param int $timestart append activity since this time
 * @param int $courseid the id of the course we produce the report for
 * @param int $cmid course module id
 * @param int $userid check for a particular user's activity only, defaults to 0 (all users)
 * @param int $groupid check for a particular group's activity only, defaults to 0 (all groups)
 * @return void adds items into $activities and increases $index
 */
function vote_get_recent_mod_activity(&$activities, &$index, $timestart, $courseid, $cmid, $userid=0, $groupid=0) {
}

/**
 * Prints single activity item prepared by {@see vote_get_recent_mod_activity()}
 *
 * @return void
 */
function vote_print_recent_mod_activity($activity, $courseid, $detail, $modnames, $viewfullnames) {
}

/**
 * Returns an array of users who are participanting in this vote
 *
 * Must return an array of users who are participants for a given instance
 * of vote. Must include every user involved in the instance,
 * independient of his role (student, teacher, admin...). The returned
 * objects must contain at least id property.
 * See other modules as example.
 *
 * @param int $voteid ID of an instance of this module
 * @return boolean|array false if no participants, array of objects otherwise
 */
function vote_get_participants($voteid) {
    global $DB;

    $params = [];
    $params['voteid'] = $voteid;
    $sql = "SELECT DISTINCT u.id FROM {role_assignments} r "
            ."JOIN {user} u ON u.id = r.userid "
            ."LEFT JOIN {vote_votes} v ON u.id = v.userid "
            ."WHERE v.voteid = :voteid";

    return $DB->get_records_sql($sql, $params);
}

/**
 * Returns all other caps used in the module
 *
 * @example return array('moodle/site:accessallgroups');
 * @return array
 */
function vote_get_extra_capabilities() {
    return [
        'mod/vote:view',
        'mod/vote:submit',
        'mod/vote:edit',
    ];
}

// File API.

/**
 * Returns the lists of all browsable file areas within the given module context
 *
 * The file area 'intro' for the activity introduction field is added automatically
 * by {@link file_browser::get_file_info_context_module()}
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @return array of [(string)filearea] => (string)description
 */
function vote_get_file_areas($course, $cm, $context) {
    return [];
}

/**
 * Serves the files from the vote file areas
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @return void this should never return to the caller
 */
function vote_pluginfile($course, $cm, $context, $filearea, array $args, $forcedownload) {
    global $DB, $CFG;

    if ($context->contextlevel != CONTEXT_MODULE) {
        send_file_not_found();
    }

    require_login($course, true, $cm);

    send_file_not_found();
}

/**
 * Obtains the automatic completion state for this vote based on any conditions
 * in vote settings.
 *
 * @param object $course Course
 * @param object $cm Course-module
 * @param int $userid User ID
 * @param bool $type Type of comparison (or/and; can be used as return value if no conditions)
 * @return bool True if completed, false if not. (If no conditions, then return
 *   value depends on comparison type)
 */
function vote_get_completion_state($course, $cm, $userid, $type) {
    global $DB;

    // Get the vote's details.
    if (!($vote = $DB->get_record('vote', ['id' => $cm->instance]))) {
        throw new Exception("Can't find vote {$cm->instance}");
    }

    $result = $type; // Default return value.

    if (!empty($vote->completionvoted)) { // Users must have voted.
        $voted = $DB->record_exists('vote_votes', ['voteid' => $vote->id, 'userid' => $userid]);

        if ($type == COMPLETION_AND) {
            $result &= $voted;
        } else {
            $result |= $voted;
        }
    }

    return $result;
}

/**
 * Get an icon mapping for font-awesome
 *
 * @return array of mappings from the icon name to the font awesome name.
 */
function mod_vote_get_fontawesome_icon_map() {
    $mapping = [
        'mod_vote:delete' => 'fa-trash',
        'mod_vote:edit' => 'fa-cog',
    ];
    return $mapping;
}

/**
 * Is the event visible?
 *
 * This is used to determine global visibility of an event in all places throughout Moodle.
 *
 * @param calendar_event $event
 * @param int $userid User id to use for all capability checks, etc. Set to 0 for current user (default).
 * @return bool Returns true if the event is visible to the current user, false otherwise.
 */
function mod_vote_core_calendar_is_event_visible(calendar_event $event, $userid = 0) {
    $visibile = true;
    $cm = get_fast_modinfo($event->courseid)->instances['vote'][$event->instance];
    $context = context_module::instance($cm->id);
    // The API expects 0 for the current user, capability checks expect null.
    $checkuser = ($userid == 0) ? $userid : null;
    if ($event->eventtype === events::CLOSE_DATE) {
        $vote = new \mod_vote\vote((int)$cm->instance);
        $visibile = has_capability('mod/vote:submit', $context, $checkuser) && $vote->is_available();
    }
    return $visibile;
}

/**
 * This function receives a calendar event and returns the action associated with it, or null if there is none.
 *
 * This is used by block_myoverview in order to display the event appropriately. If null is returned then the event
 * is not displayed on the block.
 *
 * @param calendar_event $event
 * @param \core_calendar\action_factory $factory
 * @param int $userid User id to use for all capability checks, etc. Set to 0 for current user (default).
 * @return \core_calendar\local\event\entities\action_interface|null
 */
function mod_vote_core_calendar_provide_event_action(calendar_event $event, \core_calendar\action_factory $factory, $userid = 0) {
    $cm = get_fast_modinfo($event->courseid)->instances['vote'][$event->instance];
    $vote = new \mod_vote\vote((int)$cm->instance);
    // The API expects 0 for the current user, capability checks expect null.
    $checkuser = ($userid == 0) ? $userid : null;
    if ($event->eventtype === events::CLOSE_DATE && $vote->can_submit($checkuser)) {
        $name = get_string('vote', 'mod_vote');
        $url = new \moodle_url('/mod/vote/view.php', ['id' => $cm->id]);
        $itemcount = 1;
        $actionable = true;
    } else {
        // Not an event we want to display in the timeline.
        return null;
    }
    return $factory->create_instance(
        $name,
        $url,
        $itemcount,
        $actionable
    );
}

/**
 * Callback function that determines whether an action event should be showing its item count
 * based on the event type and the item count.
 *
 * @param calendar_event $event The calendar event.
 * @param int $itemcount The item count associated with the action event.
 * @return bool
 */
function mod_vote_core_calendar_event_action_show_items_acount(calendar_event $event, $itemcount = 0) {
    return ($itemcount > 1);
}

/**
 * Callback which returns human-readable strings describing the active completion custom rules for the module instance.
 *
 * @param cm_info|stdClass $cm object with fields ->completion and ->customdata['customcompletionrules']
 * @return array $descriptions the array of descriptions for the custom rules.
 */
function mod_vote_get_completion_active_rule_descriptions($cm) {
    // Values will be present in cm_info, and we assume these are up to date.
    if (empty($cm->customdata['customcompletionrules'])
            || $cm->completion != COMPLETION_TRACKING_AUTOMATIC) {
        return [];
    }

    $descriptions = [];
    foreach ($cm->customdata['customcompletionrules'] as $key => $val) {
        switch ($key) {
            case 'completionvoted':
                if (!empty($val)) {
                    $descriptions[] = get_string('completionvoted', 'vote');
                }
                break;
            default:
                break;
        }
    }
    return $descriptions;
}

/**
 * Add a get_coursemodule_info function in case any vote type wants to add 'extra' information
 * for the course (see resource).
 *
 * Given a course_module object, this function returns any "extra" information that may be needed
 * when printing this activity in a course listing.  See get_array_of_activities() in course/lib.php.
 *
 * @param stdClass $coursemodule The coursemodule object (record).
 * @return cached_cm_info An object on information that the courses
 *                        will know about (most noticeably, an icon).
 */
function vote_get_coursemodule_info($coursemodule) {
    global $DB;

    $dbparams = ['id' => $coursemodule->instance];
    $fields = 'id, name, intro, introformat, completionvoted, closedate';
    if (!$vote = $DB->get_record('vote', $dbparams, $fields)) {
        return false;
    }

    $result = new cached_cm_info();
    $result->name = $vote->name;

    if ($coursemodule->showdescription) {
        // Convert intro to html. Do not filter cached version, filters run at display time.
        $result->content = format_module_intro('vote', $vote, $coursemodule->id, false);
    }

    // Populate the custom completion rules as key => value pairs, but only if the completion mode is 'automatic'.
    if ($coursemodule->completion == COMPLETION_TRACKING_AUTOMATIC) {
        $result->customdata['customcompletionrules']['completionvoted'] = $vote->completionvoted;
    }
    // Populate some other values that can be used in calendar or on dashboard.
    if ($vote->closedate) {
        $result->customdata['closedate'] = $vote->closedate;
    }
    return $result;
}

/**
 * Callback to fetch the activity event type lang string.
 *
 * @param string $eventtype The event type.
 * @return lang_string The event type lang string.
 */
function mod_vote_core_calendar_get_event_action_string(string $eventtype): string {
    switch ($eventtype) {
        case events::CLOSE_DATE:
            return get_string('votingcloses', 'mod_vote');
        default:
            $modulename = get_string('modulename', 'examtimer');
            return get_string('requiresaction', 'calendar', $modulename);
    }
}
