ABOUT
==========
The 'Vote' module was developed by
    Neill Magill

This module may be distributed under the terms of the General Public License
(see http://www.gnu.org/licenses/gpl.txt for details)

PURPOSE
==========
The Vote module allows you to create a simple selection of options for which students will vote.

INSTALLATION
==========
The Vote module follows the standard installation procedure.

1. Create folder <path to your moodle dir>/mod/vote.
2. Extract files from folder inside archive to created folder.
3. Visit page Home ► Site administration ► Notifications to complete installation.

DEVELOPMENT NOTES
==========
To run the Moodle app behat tests (annotated with @app) will require that the
[local_moodlemobileapp plugin](https://moodle.org/plugins/view.php?plugin=local_moodlemobileapp)
is installed in your Moodle environment.

If you do not have that plugin installed please ensure that Moodle app tests are not run during
behat if you wish to avoid failures.
